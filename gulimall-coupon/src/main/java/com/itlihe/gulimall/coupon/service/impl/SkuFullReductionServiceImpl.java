package com.itlihe.gulimall.coupon.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.to.MemberPrice;
import com.itlihe.common.to.SkuReductionTo;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.coupon.dao.SkuFullReductionDao;
import com.itlihe.gulimall.coupon.entity.MemberPriceEntity;
import com.itlihe.gulimall.coupon.entity.SkuFullReductionEntity;
import com.itlihe.gulimall.coupon.entity.SkuLadderEntity;
import com.itlihe.gulimall.coupon.service.MemberPriceService;
import com.itlihe.gulimall.coupon.service.SkuFullReductionService;
import com.itlihe.gulimall.coupon.service.SkuLadderService;


@Service("skuFullReductionService")
public class SkuFullReductionServiceImpl extends ServiceImpl<SkuFullReductionDao, SkuFullReductionEntity> implements SkuFullReductionService {

    @Autowired
    private SkuLadderService skuLadderService;

    @Autowired
    private SkuFullReductionService skuFullReductionService;

    @Autowired
    private SkuFullReductionDao skuFullReductionDao;

    @Autowired
    private MemberPriceService memberPriceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuFullReductionEntity> page = this.page(
                new Query<SkuFullReductionEntity>().getPage(params),
                new QueryWrapper<SkuFullReductionEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 被远程调用保存 //6.4) sku的优惠,满减等信息: gulimall_sms->sms_sku_ladder\sms_sku_full_reduction\sms_member_price
     * @param skuReductionTo
     */
    @Override
    public void saveSkuReduction(SkuReductionTo skuReductionTo) {

        // 1.保存到sms_sku_ladder
        /**     SkuReductionTo
         *     private Long skuId;
         *     private int fullCount;
         *     private BigDecimal discount;
         *     private int countStatus;   对应SkuLadderEntity中的AddOther
         */
        SkuLadderEntity skuLadderEntity = new SkuLadderEntity();
        BeanUtils.copyProperties(skuReductionTo,skuLadderEntity);
        skuLadderEntity.setSkuId(skuReductionTo.getSkuId());
        skuLadderEntity.setFullCount(skuReductionTo.getFullCount());
        skuLadderEntity.setDiscount(skuReductionTo.getDiscount());
        skuLadderEntity.setAddOther(skuReductionTo.getCountStatus());
        // skuLadderEntity.setPrice();   折后价 后面算订单的时候再添加
        if(skuReductionTo.getFullCount() > 0){
            skuLadderService.saveSkuReduction(skuLadderEntity);
        }

        // 2.保存到 sms_sku_full_reduction
        //          SkuReductionTo
        //      private Long skuId;
        //      private BigDecimal fullPrice;
        //     private BigDecimal reducePrice;
        //      private int priceStatus;    对应 SkuFullReductionEntity 中的 addOther
        SkuFullReductionEntity skuFullReductionEntity = new SkuFullReductionEntity();
        BeanUtils.copyProperties(skuReductionTo,skuFullReductionEntity);
        if(skuFullReductionEntity.getFullPrice().compareTo(new BigDecimal("0"))==1){
            skuFullReductionDao.saveSkuReduction(skuFullReductionEntity);
        }

        // 3.保存到 sms_member_price
        List<MemberPrice> memberPrices = skuReductionTo.getMemberPrice();

        List<MemberPriceEntity> memberPriceEntities = memberPrices.stream().map(item -> {
            MemberPriceEntity priceEntity = new MemberPriceEntity();
            // entity 和 to 中的属性名称不一样
            priceEntity.setSkuId(skuReductionTo.getSkuId());
            priceEntity.setMemberLevelId(item.getId());
            priceEntity.setMemberLevelName(item.getName());
            priceEntity.setMemberPrice(item.getPrice());
            priceEntity.setAddOther(1);
            return priceEntity;
        }).filter(item->{
            return item.getMemberPrice().compareTo(new BigDecimal("0")) == 1;
        }).collect(Collectors.toList());
        memberPriceService.saveMemberPrice(memberPriceEntities);

    }

}