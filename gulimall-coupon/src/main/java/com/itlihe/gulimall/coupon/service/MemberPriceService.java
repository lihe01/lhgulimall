package com.itlihe.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.coupon.entity.MemberPriceEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品会员价格
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-17 08:44:20
 */
public interface MemberPriceService extends IService<MemberPriceEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveMemberPrice(List<MemberPriceEntity> memberPriceEntities);

}

