package com.itlihe.gulimall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description: 优惠劵服务
 * @author: LiHe
 * @date: 2022/11/17 8:36
 * @version: 1.0
 */

/**
 * 使用nacos做配置中心的步骤
 *  1）导入依赖
 *  2）编写 bootstrap.properties
 *          # 优先级最高
 *          # 注册到配置中心的服务名称
 *          spring.application.name=gulimall-coupon
 *          # 配置中心地址
 *          spring.cloud.nacos.config.server-addr=127.0.0.1:8848
 *  3) 在nacos配置中心中添加数据集（data ID）gulimall-coupon.properties 默认规则：应用名.properties。添加配置
 *  4）添加给 controller 层添加注解
 *          @RefreshScope : 动态刷新配置注解
 *          @value("${配置名}") ：获取到注解
 *  高级配置 (说到底还是配置 Path 来找到对应的配置)  就像 feign 在nacos中注册来找到其他微服务一样，就是拼接 URL
 *  1）添加名称空间，用来隔离服务和服务配置的调用和环境的隔离：
 *   ①.开发，测试，生产环境做隔离
 *      创建对应的 dev，test，prod 命名空间之后在配置文件bootstrap.properties 中添加配置spring.cloud.nacos.config.namespace=命名空间ID
 *   ②.服务和服务之间的隔离
 *      每个微服务创建自己的命名空间，然后使用配置分组区分环境(dev/test/prod）
 *          比如优惠劵的创建就可以
 *               spring.cloud.nacos.config.namespace=优惠劵命名空间ID
 *               spring.cloud.nacos.config.group=dev
 */
@EnableFeignClients
@SpringBootApplication
@EnableDiscoveryClient
public class GuilimallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuilimallCouponApplication.class);

    }


}
