package com.itlihe.gulimall.coupon.dao;

import java.util.List;

import com.itlihe.gulimall.coupon.entity.MemberPriceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品会员价格
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-17 08:44:20
 */
@Mapper
public interface MemberPriceDao extends BaseMapper<MemberPriceEntity> {

    void saveMemberPrice(@Param("memberPriceEntities") List<MemberPriceEntity> memberPriceEntities);

}
