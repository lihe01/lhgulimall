package com.itlihe.gulimall.coupon.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.gulimall.coupon.entity.CouponEntity;
import com.itlihe.gulimall.coupon.service.CouponService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.R;


/**
 * 优惠券信息
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-17 08:44:20
 */
@RefreshScope
@RestController
@RequestMapping("coupon/coupon")
public class CouponController {
    @Autowired
    private CouponService couponService;

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private Integer port;

    /**
     * 测试获取配置文件中的值
     * @return
     */
    @GetMapping("/testConfig")
    public R testConfig(){

        // 返回配置文件中项目名称和端口号
        return R.ok().put("applicationName",applicationName).put("port",port);

    }

    /**
     * 测试使用feign实现微服务调用
     * @return
     */
    @GetMapping("/query/coupon")
    public R memberCoupon(){

        CouponEntity couponEntity = new CouponEntity();
        couponEntity.setCouponName("满50减25");

        R couponEntity1 = R.ok().put("couponEntity", Arrays.asList(couponEntity));

        return couponEntity1;

    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("coupon:coupon:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = couponService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("coupon:coupon:info")
    public R info(@PathVariable("id") Long id) {
            CouponEntity coupon = couponService.getById(id);

        return R.ok().put("coupon", coupon);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("coupon:coupon:save")
    public R save(@RequestBody CouponEntity coupon) {
            couponService.save(coupon);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("coupon:coupon:update")
    public R update(@RequestBody CouponEntity coupon) {
            couponService.updateById(coupon);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("coupon:coupon:delete")
    public R delete(@RequestBody Long[] ids) {
            couponService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
