package com.itlihe.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.to.SpuBoundTo;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-17 08:44:20
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuBoudTo(SpuBoundsEntity spuBoundsEntity);
}

