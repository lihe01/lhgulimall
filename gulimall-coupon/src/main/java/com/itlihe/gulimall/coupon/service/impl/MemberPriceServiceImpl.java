package com.itlihe.gulimall.coupon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.coupon.dao.MemberPriceDao;
import com.itlihe.gulimall.coupon.entity.MemberPriceEntity;
import com.itlihe.gulimall.coupon.service.MemberPriceService;


@Service("memberPriceService")
public class MemberPriceServiceImpl extends ServiceImpl<MemberPriceDao, MemberPriceEntity> implements MemberPriceService {

    @Autowired
    private MemberPriceDao memberPriceDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberPriceEntity> page = this.page(
                new Query<MemberPriceEntity>().getPage(params),
                new QueryWrapper<MemberPriceEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 新增
     * @param memberPriceEntities
     */
    @Override
    public void saveMemberPrice(List<MemberPriceEntity> memberPriceEntities) {
        memberPriceDao.saveMemberPrice(memberPriceEntities);
    }

}