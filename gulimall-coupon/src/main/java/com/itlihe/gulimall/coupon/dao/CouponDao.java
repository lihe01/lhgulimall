package com.itlihe.gulimall.coupon.dao;

import com.itlihe.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-17 08:44:20
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
