package com.itlihe.gulimall.coupon.dao;

import com.itlihe.gulimall.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-17 08:44:20
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {

    void saveSkuReduction(SkuLadderEntity skuLadderEntity);

}
