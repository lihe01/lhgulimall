package com.itlihe.gulimall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * @description: 解决请求跨域问题
 * @author: LiHe
 * @date: 2022/11/19 22:35
 * @version: 1.0
 */

@Configuration
public class GulimallCorsConfig {

    @Bean
    public CorsWebFilter corsWebFilter() {


        // 1. 添加cors配置信息
        CorsConfiguration config = new CorsConfiguration();

        // 设置允许请求来源
        config.addAllowedOrigin("*");

        // 设置允许请求的方式
        config.addAllowedMethod("*");

        // 设置允许的header 请求头
        config.addAllowedHeader("*");

        // 设置是否发送cookie信息
        config.setAllowCredentials(true);

        // 2. 为url添加映射路径
        UrlBasedCorsConfigurationSource corsSource = new UrlBasedCorsConfigurationSource();
        corsSource.registerCorsConfiguration("/**", config);

        // 3. 返回重新定义好的corsSource
        return new CorsWebFilter(corsSource);

    }
}