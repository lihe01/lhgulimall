package com.itlihe.gulimall.product.vo.skuitemvo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SpuBaseAttrVo {
    private String attrName;
    private String attrValue;
}
