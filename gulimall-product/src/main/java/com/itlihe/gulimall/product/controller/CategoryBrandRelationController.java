package com.itlihe.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.gulimall.product.entity.CategoryBrandRelationEntity;
import com.itlihe.gulimall.product.service.AttrGroupService;
import com.itlihe.gulimall.product.service.CategoryBrandRelationService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.R;
import com.itlihe.gulimall.product.vo.AttrAttrgroupRelationVo;
import com.itlihe.gulimall.product.vo.BrandRelationVo;


/**
 * 品牌分类关联
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@RestController
@RequestMapping("/product/categorybrandrelation")
public class CategoryBrandRelationController {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    private AttrGroupService attrGroupService;




    /**
     * 获取 分类关联的品牌
     * /product/categorybrandrelation/brands/list
     * 请求参数 cateId
     */
    @GetMapping("/brands/list")
    public R selectRelevanceBrand(@RequestParam(value = "catId", required = true) long cateId) {

        List<BrandRelationVo> brandRelationVos = categoryBrandRelationService.selectByCateId(cateId);

        return R.ok().put("data", brandRelationVos);
    }


    /**
     * 查询出品牌关联连的分类名和品牌名
     */
    @GetMapping("/catelog/list")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("product:categorybrandrelation:list")
    public R cateloglist(@RequestParam Long brandId) {

        List<CategoryBrandRelationEntity> categoryBrandRelationEntityList = categoryBrandRelationService.ralationList(brandId);

        return R.ok().put("data", categoryBrandRelationEntityList);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("product:categorybrandrelation:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = categoryBrandRelationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("product:categorybrandrelation:info")
    public R info(@PathVariable("id") Long id) {
        CategoryBrandRelationEntity categoryBrandRelation = categoryBrandRelationService.getById(id);

        return R.ok().put("categoryBrandRelation", categoryBrandRelation);
    }

    /**
     * 新增 前端传过来的是 品牌id和分类id
     */
    @RequestMapping("/save")
    // @RequiresPermissions("product:categorybrandrelation:save")
    public R save(@RequestBody CategoryBrandRelationEntity categoryBrandRelation) {
        categoryBrandRelationService.saveRelation(categoryBrandRelation);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("product:categorybrandrelation:update")
    public R update(@RequestBody CategoryBrandRelationEntity categoryBrandRelation) {
        categoryBrandRelationService.updateById(categoryBrandRelation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("product:categorybrandrelation:delete")
    public R delete(@RequestBody Long[] ids) {
        categoryBrandRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
