package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.SkuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itlihe.gulimall.product.vo.skuitemvo.SkuItemSaleAttrVo;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * sku信息
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
public interface SkuInfoDao extends BaseMapper<SkuInfoEntity> {


    List<SkuInfoEntity> queryPageByCondition(@Param("key") String key
            ,@Param("catalogId") String catalogId,@Param("brandId") String brandId
            ,@Param("max") String max,@Param("min") String min);

    List<SkuInfoEntity> selectBySpuId(Long spuId);

    List<SkuItemSaleAttrVo> queryByspuId(Long spuId);
}
