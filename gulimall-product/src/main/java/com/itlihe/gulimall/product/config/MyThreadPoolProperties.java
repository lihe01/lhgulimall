package com.itlihe.gulimall.product.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.Value;

/**
 * @description: 使用配置文件动态设置线程池
 * 还可以直接在配置文件中配置，使用@values来获取对应的值
 * @author: LiHe
 * @date: 2022/12/14 22:00
 * @version: 1.0
 */
// 属性 prefix 必须全部小写
@ConfigurationProperties(prefix = "gulimall.thread")
@Component
@Data
public class MyThreadPoolProperties {


    private Integer corePoolSize;
    private Integer maximumPoolSize;
    private Integer keepAliveTime;

}
