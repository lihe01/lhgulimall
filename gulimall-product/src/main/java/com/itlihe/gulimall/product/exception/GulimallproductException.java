package com.itlihe.gulimall.product.exception;

import java.util.HashMap;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.itlihe.common.exception.GulimallExceptionEnum;
import com.itlihe.common.utils.R;

import lombok.extern.slf4j.Slf4j;


/**
 * @description: 异常统一处理
 * @author: LiHe
 * @date: 2022/11/23 10:10
 * @version: 1.0
 */
@RestControllerAdvice(basePackages = "com.itlihe.gulimall.product.controller")
@Slf4j
// @ResponseBody  因为实体类 R是都是以json类型响应给前端的
// @ControllerAdvice(basePackages = "com.itlihe.gulimall.product.controller")
public class GulimallproductException {

    /**
     * 定义要显示的异常类型
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleVaildException(MethodArgumentNotValidException e) {
        log.info("数据校验出现问题，异常类型" + e.getClass() + e.getMessage());

        // 当我们指定了异常的具体类型的时候可以通过获取 getBindingResult 来给前端显示具体的异常信息
        BindingResult bindingResult = e.getBindingResult();

        HashMap<String, String> map = new HashMap<>();
        bindingResult.getFieldErrors().forEach((item) -> {

            map.put(item.getField(), item.getDefaultMessage());
        });

        // 使用我们公司自己规定的响应状态码和异常信息
        return R.error(GulimallExceptionEnum.UNKNOW_EXCEPTION.getCode(), GulimallExceptionEnum.VAILD_EXCEPTION.getMsg()).put("data", map);

    }

    /**
     * 匹配所有异常
     * Throwable 类是 Java 语言中所有错误或异常的超类
     */
    @ExceptionHandler(value = Throwable.class)
    public R allException(Throwable throwable) {
        log.error("错误", throwable);
        return R.error(GulimallExceptionEnum.UNKNOW_EXCEPTION.getCode(), GulimallExceptionEnum.VAILD_EXCEPTION.getMsg());

    }

}
