package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * spu信息
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {

    void saveBase(SpuInfoEntity spuInfoEntity);

    List<SpuInfoEntity> ComplexRetrieval(@Param("key") String key,@Param("catalogId") String catalogId,@Param("brandId") String brandId,@Param("status") String status);

    void updateSpuStatus(@Param("spuId") Long spuId, @Param("code") Integer code);
}
