package com.itlihe.gulimall.product.web;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.redisson.api.RCountDownLatch;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itlihe.common.utils.RedisUtil;
import com.itlihe.gulimall.product.entity.CategoryEntity;
import com.itlihe.gulimall.product.service.CategoryService;
import com.itlihe.gulimall.product.vo.Catalog2Vo;

/**
 * @description: 首页展示  在前后端不分离的项目中，响应给前端的数据一般有json和只是返回拼接的物理视图信息
 * 所以不能直接在类上加上 @ResponseBody
 * @author: LiHe
 * @date: 2022/12/6 21:38
 * @version: 1.0
 */
@Controller
public class IndexController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RedissonClient redisson;

    @Autowired
    private RedisUtil redisUtil;





    /**
     * 查询出一级分类
     *
     * @param model
     * @return
     */
    @GetMapping({"/", "/index.html"})
    public String indexShow(Model model) {

        List<CategoryEntity> categoryEntities = categoryService.menus();

        model.addAttribute("categorys", categoryEntities);

        return "index";
    }

    /**
     * 三级分类 前端需要的数据是 Map(String,List<Catalog2Vo>)
     * 获取分类json对象
     */
    @ResponseBody
    @GetMapping(value = "/index/catalog.json")
    public Map<String, List<Catalog2Vo>> getCatalogJson() {

        Map<String, List<Catalog2Vo>> catalogMap = categoryService.getCatalogJson();

        return catalogMap;

    }


    /**
     * 模拟获取相同锁
     */
    @ResponseBody
    @GetMapping("/hello")
    public String hello() {

        // 1.获取同一把锁，只要锁的名字一样，就是同一把锁
        RLock lock = redisson.getLock("my-lock");

        // 2.加锁 阻塞式等待，只有拿到锁才会执行下面的方法，自旋是直接重试看是否能拿到锁
        /**
         *  redisson 解决了锁的自动续期，如果业务超长，运行期间自动给锁续上新的30秒，不用担心业务时间太长，锁自动过期被删掉
         *          加锁的业务只要完成就不会给当前锁续期，即使不手动解锁（执行完业务突然程序崩溃还没有解锁），锁默认在30s以后自动删除
         */
        // lock.lock();

        /**
         *  如果指定了自动解锁时间，就要注意释放锁的时间，一定要大于业务执行的时间,业务在自动解锁之后，就不会自动续期了。会导致其他线程进来抢到锁
         *  1.如果不指定超时时间，就会默认使用看门狗的时间 30秒 并且还会自动续期续期时间是10秒之后
         *  2.如果指定了超时时间，会使用我们自己的时间，超时就会过期，所以最佳的实战就是设置超时时间就是30秒，这样就不会自动续期还能保证线程执行完成
         */
        lock.lock(30, TimeUnit.SECONDS);
        try {
            System.out.println("加锁成功");
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // 3.解锁不管业务成功还是失败都要解锁
            System.out.println("释放锁" + Thread.currentThread().getId());
            lock.unlock();
        }

        return "hello";
    }


    /**
     * 加上写锁 保证一定读到的是最新的数据，在修改期间，写锁是一个排它锁，读锁是一个共享锁
     * 在写锁没释放锁的情况下必须等待
     * 读 + 读：相当于无锁，并发读，只会在redis中记录好，所有当前的读锁。他们都会同时加锁成功
     * 写 + 读:等待写锁释放
     * 写 + 写:阻塞方式
     * 读 +写:有读锁。写也需要等待
     * 只要有写的存在，都必须等待
     *
     * @return
     */
    @ResponseBody
    @GetMapping("/writh")
    public String writhLock() {

        // 定义一个读写锁
        RReadWriteLock readWriteLock = redisson.getReadWriteLock("rw-lock");
        // 加上写锁
        RLock wLock = readWriteLock.writeLock();
        String uuId = "";
        try {
            // 改数据加上写锁
            wLock.lock();
            System.out.println("写锁加锁成功" + Thread.currentThread().getId());
            uuId = UUID.randomUUID().toString();
            Thread.sleep(5000);
            redisUtil.set("writeValue", uuId);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // 释放锁
            System.out.println("写锁释放锁" + Thread.currentThread().getId());
            wLock.unlock();
        }
        return uuId;
    }


    /**
     * 加上读锁
     *
     * @return
     */
    @ResponseBody
    @GetMapping("/read")
    public String readLock() {

        RReadWriteLock readWriteLock = redisson.getReadWriteLock("rw-lock");
        // 加上读锁
        RLock rLock = readWriteLock.readLock();
        rLock.lock();
        String writeValue = "";
        try {
            System.out.println("读锁加锁成功");
            writeValue = (String) redisUtil.get("writeValue");
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // 释放锁
            System.out.println("读锁释放锁");
            rLock.unlock();
        }
        return writeValue;

    }


    /**
     * 信号量锁
     * 相当于停车位 比如有三个车位，获取一个车位少一个，只有我们将锁释放才能在停车。所以可以用作限流
     *
     * @return
     */
    @ResponseBody
    @GetMapping("/park")
    public String park() {
        // 获取信号量锁 锁中设定有多少个线程
        RSemaphore park = redisson.getSemaphore("park");
        try {
            // 获取一个信号量
            // park.acquire();
            boolean acquire = park.tryAcquire(); // 判断是否还有车位
            if (acquire) {
                // 执行业务
            } else {
                return "对不起你抢购失败";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "ok";
    }

    /**
     * 开走车，释放车位，释放锁
     * @return
     */
    @ResponseBody
    @GetMapping("go")
    public String go(){
        RSemaphore park = redisson.getSemaphore("park");

        // 释放车位
        park.release();


        return "go";

    }


    /**
     * 闭锁
     * 相当于 定义的锁中的值全部释放才能运行程序：像是学校放假锁门要等学生全部出去校门，还像游戏要玩家全部加载完成才能一起进入游戏
     * @return
     */
    @ResponseBody
    @GetMapping("/door")
    public String lockDoor(){

        redisUtil.set("door",5);
        // 添加闭锁
        RCountDownLatch door = redisson.getCountDownLatch("door");
        try {
            door.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "放假了";

    }

    /**
     * 将闭锁中的值全部释放才能执行闭锁中的信息
     * @param id
     * @return
     */
    @ResponseBody
    @GetMapping("/gogo/{id}")
    public String gogo(@PathVariable("id") Integer id){

        RCountDownLatch door = redisson.getCountDownLatch("door");

        door.countDown(); // 没有指定的话每次减一

        return "放假了走人" + id;

    }

}
