/**
  * Copyright 2022 bejson.com 
  */
package com.itlihe.gulimall.product.vo.spusavevo;
import java.math.BigDecimal;
import java.util.List;

import com.itlihe.gulimall.product.vo.spusavevo.Attr;
import com.itlihe.gulimall.product.vo.spusavevo.Images;
import com.itlihe.gulimall.product.vo.spusavevo.MemberPrice;

import lombok.Data;

/**
 * Auto-generated: 2022-11-29 14:54:35
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Skus {

    // 5.1需要保持到  pms_sku_sale_attr_value 中的信息
    private List<Attr> attr;
    // 5.2需要保持到 pms_sku_info 中的信息
    private String skuName;
    private BigDecimal price;
    private String skuTitle;
    private String skuSubtitle;
    // 5.3需要保持到  pms_sku_images 中的信息
    private List<Images> images;


    // 5.4 保存优惠和满减信息是调用另一个服务中的表操作
    private List<String> descar;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPrice> memberPrice;

}