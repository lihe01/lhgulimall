package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.CategoryBrandRelationEntity;
import com.itlihe.gulimall.product.vo.BrandRelationVo;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryBrandRelationEntity> ralationList(Long brandId);

    void saveRelation(CategoryBrandRelationEntity categoryBrandRelation);

    void updateByBrandId(Long brandId, String name);

    void updateByDetail(Long catId, String name);

    List<BrandRelationVo> selectByCateId(Long cateId);
}

