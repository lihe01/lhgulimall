package com.itlihe.gulimall.product.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.product.dao.BrandDao;
import com.itlihe.gulimall.product.dao.CategoryBrandRelationDao;
import com.itlihe.gulimall.product.entity.BrandEntity;
import com.itlihe.gulimall.product.service.BrandService;
import com.itlihe.gulimall.product.service.CategoryBrandRelationService;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Autowired
    private BrandDao brandDao;

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        // 进行模糊查询
        String key = (String) params.get("key");

        List<BrandEntity> brand = brandDao.queryBykey(key);

        IPage<BrandEntity> page = this.page(new Query<BrandEntity>().getPage(params));
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(brand);

        return pageUtils;
    }

    @Override
    public BrandEntity selectByIdName(Long brandId) {

        BrandEntity brandEntity = brandDao.selectByIdName(brandId);

        return brandEntity;
    }

    /**
     * 保证品牌表和品牌分类关联表中的数据一致
     *
     * @param brand
     */
    @Transactional
    @Override
    public void updateByDetaIl(BrandEntity brand) {

        //根据品牌id修改品牌信息
        brandDao.updateBrand(brand);
        // 如果品牌名不为空
        if (!StringUtils.isEmpty(brand.getName())) {
            // 同步更新品牌表中的信息
            categoryBrandRelationService.updateByBrandId(brand.getBrandId(), brand.getName());

            // TODO 更新其他关联
        }
    }

    @Cacheable(value = "brand",key = "'brandinf:'+#root.args[0]")
    @Override
    public List<BrandEntity> getBrandsByIds(List<Long> brandIds) {

        List<BrandEntity> brandEntities = brandDao.selectByIds(brandIds);

        return brandEntities;
    }

}