package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.BrandEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    BrandEntity selectByIdName(Long brandId);

    void updateByDetaIl(BrandEntity brand);

    List<BrandEntity> getBrandsByIds(List<Long> brandIds);
}

