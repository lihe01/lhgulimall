package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.CategoryEntity;
import com.itlihe.gulimall.product.vo.Catalog2Vo;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void removeMenuById(List<Long> asList);

    Long[] getByIdCateLogPath(Long catelogId);

    CategoryEntity selectByIdName(Long catelogId);

    void updateByDetail(CategoryEntity category);

    List<CategoryEntity> menus();

    Map<String, List<Catalog2Vo>> getCatalogJson();
}

