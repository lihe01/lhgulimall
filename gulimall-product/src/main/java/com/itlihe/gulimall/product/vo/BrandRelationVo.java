package com.itlihe.gulimall.product.vo;

import lombok.Data;

/**
 * @description:
 * @author: LiHe
 * @date: 2022/11/28 16:17
 * @version: 1.0
 */
@Data
public class BrandRelationVo {

    private Long brandId;

    private String brandName;
}
