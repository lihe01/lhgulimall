package com.itlihe.gulimall.product.vo.skuitemvo;

import java.util.List;

import com.itlihe.gulimall.product.vo.spusavevo.Attr;

import lombok.Data;
import lombok.ToString;

/**
 * @description:
 * @author: LiHe
 * @date: 2022/12/13 21:50
 * @version: 1.0
 */
@Data
@ToString
public class SpuItemAttrGroupVo {
    private String groupName;
    private List<Attr> attrValues;
}
