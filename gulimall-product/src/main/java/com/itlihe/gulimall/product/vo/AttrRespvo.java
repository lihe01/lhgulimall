package com.itlihe.gulimall.product.vo;

import lombok.Data;

/**
 * @description: 响应数据类型的vo
 * @author: LiHe
 * @date: 2022/11/26 14:20
 * @version: 1.0
 */
@Data
public class AttrRespvo extends AttrVo{

    /**
     * 还多出来两个需要响应的数据
     * "catelogName": "手机/数码/手机", //所属分类名字
     * "groupName": "主体", //所属分组名字
     */
    /**
     * 分类 名称
      */
    private String catelogName;
    /**
     * 分组名称
      */
    private String groupName;

    /**
     * 修改数据时要回显表单，回显是商品的三级分类路径
     */
    private Long[] catelogPath;

}
