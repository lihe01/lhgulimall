package com.itlihe.gulimall.product.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.product.dao.SkuImagesDao;
import com.itlihe.gulimall.product.entity.SkuImagesEntity;
import com.itlihe.gulimall.product.service.SkuImagesService;


@Service("skuImagesService")
public class SkuImagesServiceImpl extends ServiceImpl<SkuImagesDao, SkuImagesEntity> implements SkuImagesService {

    @Autowired
    private SkuImagesDao skuImagesDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuImagesEntity> page = this.page(
                new Query<SkuImagesEntity>().getPage(params),
                new QueryWrapper<SkuImagesEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveBatchSpuImages(List<SkuImagesEntity> skuImagesEntities) {
        if (skuImagesEntities.size() > 0){
            skuImagesDao.saveBatchSpuImages(skuImagesEntities);
        }
    }

    @Override
    public List<SkuImagesEntity> selectBySkuId(Long skuId) {

        List<SkuImagesEntity> skuImagesEntities = skuImagesDao.selectBySkuId(skuId);

        return skuImagesEntities;

    }

}