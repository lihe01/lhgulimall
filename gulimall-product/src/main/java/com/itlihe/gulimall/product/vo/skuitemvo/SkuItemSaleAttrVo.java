package com.itlihe.gulimall.product.vo.skuitemvo;

import java.util.List;

import lombok.Data;
import lombok.ToString;

/**
 * @description:sku销售属性vo
 * @author: LiHe
 * @date: 2022/12/13 21:44
 * @version: 1.0
 */
@Data
@ToString
public class SkuItemSaleAttrVo {
    private Long attrId;
    private String attrName;
    private List<AttrValueWithSkuIdVo> attrValues;

}
