package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 品牌
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Repository
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {

    List<BrandEntity> queryBykey(String key);

    BrandEntity selectByIdName(Long brandId);

    void updateBrand(BrandEntity brand);

    List<BrandEntity> selectByIds(@Param("brandIds") List<Long> brandIds);
}
