package com.itlihe.gulimall.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.common.group.UpdateGroup;
import com.itlihe.common.valid.AddGroup;
import com.itlihe.common.valid.UpdateStatusGroup;
import com.itlihe.common.valid.Updategroup;
import com.itlihe.gulimall.product.entity.BrandEntity;
import com.itlihe.gulimall.product.service.BrandService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.R;


/**
 * 品牌
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@RestController
@RequestMapping("product/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("product:brand:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 根据 品牌id的 list集合查询出 对应的brandEntitiy
     * @param brandIds
     * @return
     */
    @GetMapping("/infos")
    public R info(@RequestParam("brandIds") List<Long> brandIds){
        List<BrandEntity>  brands = brandService.getBrandsByIds(brandIds);
        return R.ok().put("brands",brands);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{brandId}")
    // @RequiresPermissions("product:brand:info")
    public R info(@PathVariable("brandId") Long brandId) {
            BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }

    /**
     * 保存 异常交给一个接口统一处理
     */
    @RequestMapping("/save")
    // @RequiresPermissions("product:brand:save")
    public R save(@Validated({AddGroup.class}) @RequestBody BrandEntity brand/*, BindingResult bindingResult*/) {
        // 当用户输入的数据和校验的数据格式不同，要给出提示和转到错误信息界面
        // 在@Validated注解修饰的bean之后, 紧跟Errors(或BindingResult)类型的参数
        // if (bindingResult.hasErrors()) {
        //     HashMap<String, String> Map = new HashMap<>();
        //     // 获取全部错误信息
        //     bindingResult.getFieldErrors().forEach((item)->{
        //
        //         // 获取错误提示
        //         String message = item.getDefaultMessage();
        //         // 获取错误属性的名字
        //         String field = item.getField();
        //
        //         // 也可以直接放值
        //         Map.put(message,field);
        //
        //     });
        //
        //     return R.error(400,"提交的数据不合法").put("data",Map);
        //
        // }else {
        //     brandService.save(brand);
        // }
        brandService.save(brand);
        return R.ok();
    }



    /**
     * 修改
     * 修改的时候不能只修改品牌表和品牌分类关联表也要修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("product:brand:update")
    public R update(@Validated({Updategroup.class}) @RequestBody BrandEntity brand) {

            brandService.updateByDetaIl(brand);

        return R.ok();
    }

    /**
     * 修改品牌的状态显示对应
     * @param
     * @return
     */
    @PostMapping("/update/status")
    public R updateStatus(@Validated({UpdateStatusGroup.class}) @RequestBody BrandEntity brand){

        // 根据传入的id修改对应的品牌状态
        brandService.updateById(brand);

        return R.ok();

    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("product:brand:delete")
    public R delete(@RequestBody Long[] brandIds) {
            brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }

}
