package com.itlihe.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.gulimall.product.entity.CategoryEntity;
import com.itlihe.gulimall.product.service.CategoryService;
import com.itlihe.common.utils.R;


/**
 * 商品三级分类
 * （就像京东的页面：手机一级分类，手机下面还有是游戏手机还是老年机二级分类，下面还有手机品牌三级分类）
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@RestController
@RequestMapping("product/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 查出所以分类和子分类，以树状结构组起来
     */
    @GetMapping("/list/tree")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("product:category:list")
    public R list() {

        List<CategoryEntity> categoryList = categoryService.listWithTree();

        return R.ok().put("data", categoryList);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{catId}")
    // @RequiresPermissions("product:category:info")
    public R info(@PathVariable("catId") Long catId) {
        CategoryEntity category = categoryService.getById(catId);

        // 前端统一使用data
        return R.ok().put("data", category);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("product:category:save")
    public R save(@RequestBody CategoryEntity category) {
        categoryService.save(category);

        return R.ok();
    }

    /**
     * 修改分类表的时候也要修改和他关联的品牌分类表
     */
    @RequestMapping("/update")
    // @RequiresPermissions("product:category:update")
    public R update(@RequestBody CategoryEntity category) {
        categoryService.updateByDetail(category);

        return R.ok();
    }

    /**
     * 删除 三级分类的删除，post请求 @RequestBody 获取请求体中的数据,会将请求体的json转成对应的实体对象
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("product:category:delete")
    public R delete(@RequestBody Long[] catIds) {

        // Arrays.asList(catIds) 数组转成list
        //   categoryService.removeByIds(Arrays.asList(catIds));
        // 1.检查当前删除的菜单，看是否被其他地方引用，如何引用不能直接删除
        categoryService.removeMenuById(Arrays.asList(catIds));

        return R.ok();
    }

}
