package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.AttrEntity;
import com.itlihe.gulimall.product.vo.AttrRespvo;
import com.itlihe.gulimall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttrvo(AttrVo attr);

    PageUtils queryPageBykey(Map<String, Object> params, Long catelogId,String attrtype);

    AttrRespvo getByIdDetaill(Long attrId);

    void updateByIdDetail(AttrVo attrVo);

    List<AttrEntity> queryRelevancy(Long attrgroupId);

    PageUtils selectNoRelevance(Map<String, Object> params, Long attrgroupId);

    List<Long> getBYAttrIds(List<Long> attrIds);
}

