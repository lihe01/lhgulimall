package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itlihe.gulimall.product.entity.SpuInfoDescEntity;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * spu图片
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {

    void saveImages(@Param("spuImagesEntities") List<SpuImagesEntity> spuImagesEntities);
}
