package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.ProductAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * spu属性值
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
public interface ProductAttrValueDao extends BaseMapper<ProductAttrValueEntity> {

    void saveBaseAttrs(@Param("productAttrValueEntities") List<ProductAttrValueEntity> productAttrValueEntities);

    void deleteBySpuId(Long spuId);

    List<ProductAttrValueEntity> selectBySpuId(Long spuId);

    void saveProductAttr(@Param("productAttrValueEntities") List<ProductAttrValueEntity> productAttrValueEntities);
}
