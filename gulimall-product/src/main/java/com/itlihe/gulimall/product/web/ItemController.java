package com.itlihe.gulimall.product.web;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.itlihe.gulimall.product.service.SkuInfoService;
import com.itlihe.gulimall.product.vo.skuitemvo.SkuItemVo;

/**
 * @description: 详情页
 * @author: LiHe
 * @date: 2022/12/13 21:20
 * @version: 1.0
 */
@Controller
public class ItemController {

    @Autowired
    private SkuInfoService skuInfoService;

    /**
     * 根据skuId查询出对应的商品信息
     *
     * @param skuId
     * @param model
     * @return
     */
    @GetMapping("/{skuId}.html")
    public String itemInfo(@PathVariable("skuId") Long skuId, Model model) {

        SkuItemVo item = null;
        try {
            item = skuInfoService.queryItemInfo(skuId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("item", item);

        return "item";
    }


}
