package com.itlihe.gulimall.product.vo;

import lombok.Data;

/**
 * @description: 属性分组关联 Vo 用来当我们在属性分组点击关联关系并且点击移除时需要接受的参数
 * @author: LiHe
 * @date: 2022/11/27 19:39
 * @version: 1.0
 */
@Data
public class AttrgroupRelationVo {

    // [{"attrId":1,"attrGroupId":2}]

    private Long attrId;

    private Long attrGroupId;

}
