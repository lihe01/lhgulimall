package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.SkuInfoEntity;
import com.itlihe.gulimall.product.vo.skuitemvo.SkuItemSaleAttrVo;
import com.itlihe.gulimall.product.vo.skuitemvo.SkuItemVo;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * sku信息
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveInfo(SkuInfoEntity skuInfoEntity);

    PageUtils queryPageSkuSearching(Map<String, Object> params);

    List<SkuInfoEntity> selectBySpuId(Long spuId);

    SkuItemVo queryItemInfo(Long skuId) throws ExecutionException, InterruptedException;

    List<SkuItemSaleAttrVo> queryByspuId(Long spuId);
}

