package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itlihe.gulimall.product.vo.skuitemvo.SpuItemAttrGroupVo;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 商品属性
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
@Repository
public interface AttrDao extends BaseMapper<AttrEntity> {

    void saveattrvo(AttrEntity attrEntity);

    List<AttrEntity> queryPageBykey(@Param("key") String key,@Param("catelogId") Long catelogId,@Param("type") Integer type);

    AttrEntity getByIdDetaill(Long attrId);

    void updateAttr(AttrEntity attrEntity);

    List<AttrEntity> selectByIds(@Param("attrId") List<Long> attrId);

    List<AttrEntity> selectBycatelobIdAndAttrId(@Param("catelogId") Long catelogId,@Param("attrIds") List<Long> attrIds,@Param("key") String key);


    List<AttrEntity> selectByCatrlogId(Long catelogId);

    List<Long> sleectBySearchId(@Param("attrIds") List<Long> attrIds);

    List<SpuItemAttrGroupVo> selectByspuId(@Param("spuId") Long spuId, @Param("catalogId") Long catalogId);
}
