package com.itlihe.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.gulimall.product.constant.CommonConstant;
import com.itlihe.gulimall.product.dao.AttrGroupDao;
import com.itlihe.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.itlihe.gulimall.product.entity.AttrEntity;
import com.itlihe.gulimall.product.entity.AttrGroupEntity;
import com.itlihe.gulimall.product.service.AttrAttrgroupRelationService;
import com.itlihe.gulimall.product.service.AttrGroupService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.R;
import com.itlihe.gulimall.product.service.AttrService;
import com.itlihe.gulimall.product.service.CategoryService;
import com.itlihe.gulimall.product.vo.AttrAttrgroupRelationVo;
import com.itlihe.gulimall.product.vo.AttrRespvo;
import com.itlihe.gulimall.product.vo.AttrgroupRelationVo;


/**
 * 属性分组
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    /**
     * 获取分类下所有分组&关联属性 分类下的 所有属性分组和属性信息
     * 需要响应的数据包括
     */
    @GetMapping("/{catelogId}/withattr")
    public R selectWithattr(@PathVariable("catelogId") Long catelogId) {

        // 1.查询出分类id 下的全部属性分组属性
        // 2.在查询出分类ID下的全部属性
        List<AttrAttrgroupRelationVo> relationVos = attrGroupService.selectBYcatelogId(catelogId);

        return R.ok().put("data", relationVos);

    }

    /**
     * 新增属性分组和属性的关联关系
     */
    @PostMapping("/attr/relation")
    public R saveAttrRelation(@RequestBody AttrgroupRelationVo[] vos){

        attrAttrgroupRelationService.saveAttrRelation(vos);
        return R.ok();

    }


    /**
     * 回显属性分组没有关联的其他属性
     * @return
     */
    @GetMapping("/{attrgroupId}/noattr/relation")
    public R attrNoRelevance(@RequestParam Map<String, Object> params,@PathVariable("attrgroupId") Long attrgroupId){

        PageUtils page = attrService.selectNoRelevance(params,attrgroupId);

        return R.ok().put("page",page);

    }

    /**
     * 移除属性分组和属性的关联关系
     * 前端传过来的是数组
     *
     * @param relationVo
     * @return
     */
    @PostMapping("/attr/relation/delete")
    public R deleteRelevancy(@RequestBody AttrgroupRelationVo[] relationVo) {

        attrGroupService.deleteRelevancy(relationVo);

        return R.ok();

    }


    /**
     * 显示和属性表关联关系
     * 回显属性表中的信息
     *
     * @return
     */
    @GetMapping("/{attrgroupId}/attr/relation")
    public R relevancy(@PathVariable("attrgroupId") Long attrgroupId) {

        // 获取到的值都是属性实体类中的数据
        List<AttrEntity> attrEntity = attrService.queryRelevancy(attrgroupId);

        return R.ok().put("data", attrEntity);

    }

    /**
     * 列表
     * 是根据所属分类的 catelogId 进行查询，当 catelogId == 0时说明是查询的一级目录所以查询全部，当不等于 0 时，只查询 商品等于 catelogId的值
     */
    @RequestMapping("/list/{catelogId}")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params, @PathVariable("catelogId") long catelogId) {
        // PageUtils page = attrGroupService.queryPage(params);

        PageUtils page = attrGroupService.queryPage(params, catelogId);

        return R.ok().put(CommonConstant.PAGE, page);
    }


    /**
     * 信息 表单回显
     * 获取三级分类回显的数据 （传入的是attrGroupId 第三级的id要以此获取它的父id组装成数组返回）
     */
    @RequestMapping("/info/{attrGroupId}")
    // @RequiresPermissions("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId) {

        // 获取分组id获取商品的全部分组信息
        AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        AttrRespvo attrRespvo = new AttrRespvo();

        BeanUtils.copyProperties(attrGroup, attrRespvo);
        // categoryService获取商品三级分类的信息
        Long[] catelogPath = categoryService.getByIdCateLogPath(attrGroup.getCatelogId());

        // attrGroup.setCatelogPath(catelogPath);

        attrRespvo.setCatelogPath(catelogPath);

        return R.ok().put(CommonConstant.ATTR_GROUP, attrRespvo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("product:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds) {
        attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
