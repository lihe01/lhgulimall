package com.itlihe.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.gulimall.product.entity.AttrEntity;
import com.itlihe.gulimall.product.entity.ProductAttrValueEntity;
import com.itlihe.gulimall.product.service.AttrService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.R;
import com.itlihe.gulimall.product.service.ProductAttrValueService;
import com.itlihe.gulimall.product.vo.AttrRespvo;
import com.itlihe.gulimall.product.vo.AttrVo;


/**
 * 商品属性
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;

    @Autowired
    private ProductAttrValueService productAttrValueService;


    /**
     *规格批量更新方法
     * ///product/attr/update/{spuId}
     */
    @PostMapping("/update/{spuId}")
    public R updateSpuAttr(@PathVariable("spuId") Long spuId,
                           @RequestBody List<ProductAttrValueEntity> productAttrValueEntityList){
        productAttrValueService.batchUpdateSpuAttr(spuId,productAttrValueEntityList);
        return R.ok();
    }



    /**
     * spu规格维护功能
     * /product/attr/base/listforspu/{spuId}
     */
    @GetMapping("/base/listforspu/{spuId}")
    public R baseAttrlistforspu(@PathVariable("spuId") Long spuId){
        //根据spuId查询商品属性值表,获取到该spu下所有规格返回进行回显
        List<ProductAttrValueEntity> productAttrValueEntityList = productAttrValueService.selectBySpuId(spuId);
        return R.ok().put("data",productAttrValueEntityList);
    }


    /**
     * 销售属性不需要做分组的事情
     * 查询数据
     * 商品属性有两种销售属性和基本属性
     * 什么叫基本属性或者叫规格参数？
     * 他们其实就是SPU对应的属性，也就是SKU他们都有的属性，在java里面就可以把他们看成是static类属性，跟类走的。
     * 什么叫销售属性？
     * 他们就是SKU独特有的属性，在java里面就可以把他们看成是私有属性，跟着实体走的。
     * 他们俩个在数据库表中对应的值是一样的 attr_type 属性类型[0-销售属性，1-基本属性
     * 前端传过来的是{attrType}： let type = this.attrtype == 0 ? 'sale' : 'base'
     */
    @GetMapping("/{attrType}/list/{catelogId}")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("product:attr:list")
    public R baselist(@RequestParam Map<String, Object> params,
                      @PathVariable("catelogId") Long catelogId,
                      @PathVariable("attrType") String attrType) {

        PageUtils page = attrService.queryPageBykey(params, catelogId, attrType);

        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("product:attr:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 回显表单数据，回显中除了属性表中的信息，还需要商品完整路径，商品属性分组id  attrGroupId
     */
    @GetMapping("/info/{attrId}")
    // @RequiresPermissions("product:attr:info")
    public R info(@PathVariable("attrId") Long attrId) {

        AttrRespvo attr = attrService.getByIdDetaill(attrId);

        return R.ok().put("attr", attr);
    }

    /**
     * 新增
     * 销售属性不包括分组
     */
    @PostMapping("/save")
    // @RequiresPermissions("product:attr:save")
    public R save(@RequestBody AttrVo attr) {
        attrService.saveAttrvo(attr);

        return R.ok();
    }

    /**
     * 修改 修改的时候属性表中的数据修改，也要修改到 属性&属性分组关联 表中的相关数据
     * 修改的时候多出来一个参数 attrGroupId 所以需要使用Attrvo来响应前端的数据
     */
    @RequestMapping("/update")
    // @RequiresPermissions("product:attr:update")
    public R update(@RequestBody AttrVo attrVo) {
        attrService.updateByIdDetail(attrVo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("product:attr:delete")
    public R delete(@RequestBody Long[] attrIds) {
        attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
