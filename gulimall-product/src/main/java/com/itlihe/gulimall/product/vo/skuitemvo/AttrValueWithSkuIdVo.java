package com.itlihe.gulimall.product.vo.skuitemvo;

import lombok.Data;
import lombok.ToString;

/**
 * @description:
 * @author: LiHe
 * @date: 2022/12/13 22:01
 * @version: 1.0
 */
@Data
@ToString
public class AttrValueWithSkuIdVo {

    private String attrValue;
    private String skuIds;
}
