package com.itlihe.gulimall.product.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.product.dao.CategoryBrandRelationDao;
import com.itlihe.gulimall.product.dao.CategoryDao;
import com.itlihe.gulimall.product.entity.BrandEntity;
import com.itlihe.gulimall.product.entity.CategoryBrandRelationEntity;
import com.itlihe.gulimall.product.entity.CategoryEntity;
import com.itlihe.gulimall.product.service.BrandService;
import com.itlihe.gulimall.product.service.CategoryBrandRelationService;
import com.itlihe.gulimall.product.service.CategoryService;
import com.itlihe.gulimall.product.vo.BrandRelationVo;


@Service("categoryBrandRelationService")
public class CategoryBrandRelationServiceImpl extends ServiceImpl<CategoryBrandRelationDao, CategoryBrandRelationEntity> implements CategoryBrandRelationService {

    @Autowired
    private CategoryBrandRelationDao categoryBrandRelationDao;

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryDao categoryDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryBrandRelationEntity> page = this.page(
                new Query<CategoryBrandRelationEntity>().getPage(params),
                new QueryWrapper<CategoryBrandRelationEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryBrandRelationEntity> ralationList(Long brandId) {

        List<CategoryBrandRelationEntity> categoryBrandRelationEntityList = categoryBrandRelationDao.ralationList(brandId);

        return categoryBrandRelationEntityList;
    }

    /**
     * 新增
     *
     * @param categoryBrandRelation
     */
    @Override
    public void saveRelation(CategoryBrandRelationEntity categoryBrandRelation) {
        // 从前端传过来的是品牌id和分类id
        // 由于新增的时候会从前端可以获取分类id和品牌id,所以在保存得时候需要通过两种id把两个分类名和品牌名分别查询出来一起保存到品牌分类关系表
        Long brandId = categoryBrandRelation.getBrandId();
        Long catelogId = categoryBrandRelation.getCatelogId();

        if (catelogId != null && brandId != null) {
            BrandEntity brandEntity = brandService.selectByIdName(brandId);
            CategoryEntity categoryEntity = categoryService.selectByIdName(catelogId);
            // 通过品牌实体类和分类实体类获取品牌名和分类名
            categoryBrandRelation.setBrandName(brandEntity.getName());
            categoryBrandRelation.setCatelogName(categoryEntity.getName());

            categoryBrandRelationDao.saveRelation(categoryBrandRelation);
        }


    }

    /**
     * 根据传过来的 brandId 和 brandname 同时来修改关联的表属性
     *
     * @param brandId
     * @param name
     */
    @Override
    public void updateByBrandId(Long brandId, String name) {

        categoryBrandRelationDao.updateByBrandId(brandId, name);

    }

    /**
     * 修改关联表name
     *
     * @param catId
     * @param name
     */
    @Override
    public void updateByDetail(Long catId, String name) {

        categoryBrandRelationDao.updateByDetail(catId, name);

    }

    /**
     * 查询出来分类关联的品牌
     *
     * @param cateId
     * @return
     */
    @Override
    public List<BrandRelationVo> selectByCateId(Long cateId) {


        // 查询出来的数据是数组类型的
        List<CategoryBrandRelationEntity> categoryBrandRelations = categoryBrandRelationDao.selectByCateId(cateId);

        // 转成list集合之后使用stream
        List<BrandRelationVo> brandRelationVos = categoryBrandRelations.stream().map((categoryBrandRelation) -> {

            BrandRelationVo brandRelationVo = new BrandRelationVo();
            BeanUtils.copyProperties(categoryBrandRelation, brandRelationVo);

            return brandRelationVo;

        }).collect(Collectors.toList());

        return brandRelationVos;
    }

}