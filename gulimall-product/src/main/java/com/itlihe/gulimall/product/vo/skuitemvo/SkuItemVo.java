package com.itlihe.gulimall.product.vo.skuitemvo;

import java.util.List;

import com.itlihe.gulimall.product.entity.SkuImagesEntity;
import com.itlihe.gulimall.product.entity.SkuInfoEntity;
import com.itlihe.gulimall.product.entity.SpuInfoDescEntity;

import lombok.Data;
import lombok.ToString;

/**
 * @description: sku 详情vo
 * @author: LiHe
 * @date: 2022/12/13 21:46
 * @version: 1.0
 */
@Data
@ToString
public class SkuItemVo {

    /**
     * 包含sku基本信息
     */
    private SkuInfoEntity info;
    /**
     * sku图片信息
     */
    private List<SkuImagesEntity> imagesEntities;

    /**
     * spu销售属性组合
     */
    private List<SkuItemSaleAttrVo> saleAttr;

    /**
     * spu详细信息
     */
    private SpuInfoDescEntity deso;

    /**
     * 规格参数
     */
    private List<SpuItemAttrGroupVo> groupAttrs;


}
