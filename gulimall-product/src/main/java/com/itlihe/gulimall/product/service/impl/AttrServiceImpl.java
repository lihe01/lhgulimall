package com.itlihe.gulimall.product.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.myenum.ProductAttrEnum;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.itlihe.gulimall.product.dao.AttrDao;
import com.itlihe.gulimall.product.dao.AttrGroupDao;
import com.itlihe.gulimall.product.dao.CategoryDao;
import com.itlihe.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.itlihe.gulimall.product.entity.AttrEntity;
import com.itlihe.gulimall.product.entity.AttrGroupEntity;
import com.itlihe.gulimall.product.entity.CategoryEntity;
import com.itlihe.gulimall.product.service.AttrAttrgroupRelationService;
import com.itlihe.gulimall.product.service.AttrService;
import com.itlihe.gulimall.product.service.CategoryService;
import com.itlihe.gulimall.product.vo.AttrRespvo;
import com.itlihe.gulimall.product.vo.AttrVo;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Autowired
    private AttrDao attrDao;

    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    @Autowired
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private AttrGroupDao attrGroupDao;

    @Autowired
    private CategoryService categoryService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 规格参数新增
     * 同时级联属性分组表中的数据
     *
     * @param attr
     */
    @Transactional
    @Override
    public void saveAttrvo(AttrVo attr) {

        // 1.由于属性vo类比属性类多一个属性分组id,所以在进行保存的时候还需要把分组id等保存到属性分组关系表中
        // AttrVo 是前端传过来的数据比AttrEntity 多了一个  AttrGroupId 保存的时候还是保存AttrEntity
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        attrDao.saveattrvo(attrEntity);


        // 判断是销售属性还是规格参数 销售属性没有分组 不需要级联保存
        if (attr.getAttrType().equals(ProductAttrEnum.ATTR_TYPE_BASE.getCode())) {
            // 2.级联的保存
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
            // 只需要保存这两个值
            attrAttrgroupRelationEntity.setAttrGroupId(attr.getAttrGroupId());

            // 这个 AttrId 前端没有传过来，是自动生成的所有要从，生成的实体类中拿到
            attrAttrgroupRelationEntity.setAttrId(attrEntity.getAttrId());

            attrAttrgroupRelationService.saveaAttr(attrAttrgroupRelationEntity);
        }

    }

    /**
     * 三级分类显示全部数据，并且添加模糊查询
     *
     * @param params
     * @param catelogId
     * @return
     */
    @Override
    public PageUtils queryPageBykey(Map<String, Object> params, Long catelogId, String attrType) {

        // 获取索引关键字
        String key = (String) params.get("key");

        // 因为销售属性和规格参数需要获取的值一样所以共享一个方法，只需要判断 attrType 的值是1规格参数还是0销售属性 我们定义一个枚举类
        Integer type = "base".equalsIgnoreCase(attrType) ? ProductAttrEnum.ATTR_TYPE_BASE.getCode() : ProductAttrEnum.ATTR_TYPR_SALE.getCode();

        // 根据 catelogId 查询出全部属性表中的数据，通过key来模糊查询
        List<AttrEntity> attrEntities = attrDao.queryPageBykey(key, catelogId, type);

        //  前端需要返回的数据包含属性表中的信息，还需要商品分类名和属性分组名
        List<AttrRespvo> attrRespvos = attrEntities.stream().map((attrEntity) -> {

            AttrRespvo attrRespvo = new AttrRespvo();
            //  将attrEntity的数据拷贝到attrRespvo中
            BeanUtils.copyProperties(attrEntity, attrRespvo);

            // 只有当属性类型为基本属性的时候才会有分组名和分组id
            if ("base".equalsIgnoreCase(attrType)) {
                // 1.根据 属性id获取属性分组关联表中的 属性分组id，在通过属性分组id获取到属性分组名
                AttrAttrgroupRelationEntity relationEntity = attrAttrgroupRelationDao.selectByAttrId(attrEntity.getAttrId());
                // 在这里判断不为空是因为查询 attrGroupEntity 是用的 pules
                if (relationEntity != null && relationEntity.getAttrGroupId() != null) {
                    AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(relationEntity.getAttrGroupId());
                    // 通过属性分组id获取 属性分组表，在通过属性分组表中的信息获取属性分组名字
                    if (attrGroupEntity != null && attrGroupEntity.getAttrGroupName() != null) {
                        attrRespvo.setGroupName(attrGroupEntity.getAttrGroupName());
                    }
                }
            }
            // 2.通过attrEntity 获取分类id 在通过分类id查询分类表获取分类名称，将分类名称set 到attrRespvo中
            CategoryEntity categoryEntity = categoryDao.selectById(attrEntity.getCatelogId());
            if (categoryEntity != null && categoryEntity.getName() != null) {
                attrRespvo.setCatelogName(categoryEntity.getName());
            }
            return attrRespvo;
            // 将数据收集起来
        }).collect(Collectors.toList());

        // 获取分页
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params));

        PageUtils pageUtils = new PageUtils(page);

        // 将需要返回给前端的数据添加到分页插件的setList中
        pageUtils.setList(attrRespvos);

        return pageUtils;
    }

    /**
     * 获取商品属性信息和attrgroudId catelogPath[]
     *
     * @param attrId
     * @return
     */
    @Cacheable(value = "attr",key = "'attrinf:'+#root.args[0]")
    @Override
    public AttrRespvo getByIdDetaill(Long attrId) {

        // 可以使用mybatis_pules的 selectById 来查询也可以直接sql查询
        // AttrEntity attrEntities = attrDao.selectById(attrId);
        AttrEntity attrEntity = attrDao.getByIdDetaill(attrId);
        AttrRespvo attrRespvo = new AttrRespvo();
        // 将查询出来的数据封装到Attrrespvo中
        BeanUtils.copyProperties(attrEntity, attrRespvo);

        // 1.查询出AttrGroupId和attrgroupName
        if (!ObjectUtils.isEmpty(attrEntity)) {
            // TODO 这里写错了，应该使用级联表来获取对应的属性分组id，因为在属性分组表中catelogId 不是唯一的获取的是集合
            // AttrGroupEntity attrGroupEntity = attrGroupDao.selectByCatelogIId(attrEntity.getCatelogId());
            AttrAttrgroupRelationEntity relationEntity = attrAttrgroupRelationDao.selectById(attrEntity.getCatelogId());
            // attrRespvo.setAttrGroupId(attrGroupEntity.getAttrGroupId());
            attrRespvo.setAttrGroupId(relationEntity.getAttrGroupId());
            // attrRespvo.setGroupName(attrGroupEntity.getAttrGroupName());
        }


        // 2.获取三级分类id
        Long catelonId = attrEntity.getCatelogId();

        Long[] byIdCateLogPath = categoryService.getByIdCateLogPath(catelonId);

        if (byIdCateLogPath != null) {
            attrRespvo.setCatelogPath(byIdCateLogPath);
        }

        return attrRespvo;
    }

    /**
     * 修改属性
     * 并且级联修改
     *
     * @param attrVo
     */
    @Transactional
    @Override
    public void updateByIdDetail(AttrVo attrVo) {

        AttrEntity attrEntity = new AttrEntity();
        // 将前端传过来的修改数据传入属性实体类中
        BeanUtils.copyProperties(attrVo, attrEntity);

        // 1.先将只是属性实体类的数据修改
        attrDao.updateAttr(attrEntity);

        // 判断是销售属性还是基本属性 销售属性不需要分组
        if (attrVo.getAttrType().equals(ProductAttrEnum.ATTR_TYPE_BASE.getCode())) {

            // 2.修改关联表中的数据 需要修改的是 attr_group_id 和 attr_id
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            // 直接给后端传入级联关系的实体类
            relationEntity.setAttrGroupId(attrVo.getAttrGroupId());
            relationEntity.setAttrId(attrVo.getAttrId());

            //由于在属性在添加的时候有可能存在没有选择属性分组的情况,所以我们需要进行判断,如果没有就保存,有就修改
            // 判断方法就是执行sql查询看返回的是一条语句还是没有语句
            Integer count = attrAttrgroupRelationDao.countByAttrId(attrVo.getAttrId());
            if (count > 0) {
                // 执行更新
                attrAttrgroupRelationDao.updateById(relationEntity);
            } else {
                // 执行保存
                attrAttrgroupRelationDao.insert(relationEntity);
            }
        }

    }

    /**
     * 查询出属性关联关系 属性分组
     *
     * @param attrgroupId
     * @return
     */
    @Override
    public List<AttrEntity> queryRelevancy(Long attrgroupId) {

        // 第一种方法
        // 先将关联的属性&属性分组表中的全部数据查询出来
        // List<AttrAttrgroupRelationEntity> attrgroupRelationEntities = attrAttrgroupRelationDao.selectByAttrgroupId(attrgroupId);
        //
        // // 将查询出来的全部数据，经过stream流的map映射将相当于遍历 attrgroupRelationEntities 来获取对应的AttrId 在通过 attrDao.selectById 查询出来属性数据
        // List<AttrEntity> attrEntities = attrgroupRelationEntities.stream().map((attrgroupRelationEntity) ->
        //
        //         attrDao.selectById(attrgroupRelationEntity.getAttrId())
        //
        // ).collect(Collectors.toList());


        // 第二种方法
        // 另一种写法通过gttrgroupId 获取集合 attrID 在通过foreach的方式 sql 查询出来
        List<Long> attrId = attrAttrgroupRelationDao.selectByAttrgroupId(attrgroupId).stream().map((item) -> {

            return item.getAttrId();
        }).collect(Collectors.toList());

        List<AttrEntity> attrEntityList = attrDao.selectByIds(attrId);

        return attrEntityList;
    }

    /**
     * 获取当前属性分组没有关联的属性
     *
     * @param params
     * @param attrgroupId
     * @return
     */
    @Override
    public PageUtils selectNoRelevance(Map<String, Object> params, Long attrgroupId) {

        // 1.当前属性分组只能关联自己所属分类里面的所有属性
        // 获取当前所在的分组
        AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrgroupId);

        // 2.当前分组只能选择继续关联没有被任何分组(包括自己)没有引用的属性
        // 2.1 获取当前分组下的所有分组 id
        List<AttrGroupEntity> attrGroupEntityList = attrGroupDao.selectByCatelogIId(attrGroupEntity.getCatelogId());
        List<Long> attrGroupIds = attrGroupEntityList.stream().map((attrGroup) -> {
            return attrGroup.getAttrGroupId();
        }).collect(Collectors.toList());

        // 2.2 获取被分组关联的属性
        List<AttrAttrgroupRelationEntity> relationEntities = attrAttrgroupRelationDao.selectByGroupIds(attrGroupIds);
        List<Long> attrIds = relationEntities.stream().map((relationEntity) -> {
            return relationEntity.getAttrId();
        }).collect(Collectors.toList());

        // 获取检索信息
        String key = (String) params.get("key");

        // 2.3从所有属性中移除被关联的这些属性 同时支持索引信息 在sql中判断是否需要检索 还需要将销售属性排除
        List<AttrEntity> attrEntityList = attrDao.selectBycatelobIdAndAttrId(attrGroupEntity.getCatelogId(), attrIds,key);


        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params));

        PageUtils pageUtils = new PageUtils(page);

        pageUtils.setList(attrEntityList);

        return pageUtils;


    }

    /**
     * 查询出可以被检索的属性id
     * @param attrIds
     * @return
     */
    @Override
    public List<Long> getBYAttrIds(List<Long> attrIds) {

        List<Long> SearchAttrs = attrDao.sleectBySearchId(attrIds);


        return SearchAttrs;
    }
}