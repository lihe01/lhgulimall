package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itlihe.gulimall.product.vo.skuitemvo.SkuItemSaleAttrVo;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * sku销售属性&值
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
public interface SkuSaleAttrValueDao extends BaseMapper<SkuSaleAttrValueEntity> {

    void saveValue(@Param("valueEntities") List<SkuSaleAttrValueEntity> valueEntities);

    List<SkuItemSaleAttrVo> queryByspuId(Long spuId);
}
