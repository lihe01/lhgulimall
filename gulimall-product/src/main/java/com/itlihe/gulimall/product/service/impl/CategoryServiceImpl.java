package com.itlihe.gulimall.product.service.impl;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.common.utils.RedisUtil;
import com.itlihe.gulimall.product.dao.CategoryDao;
import com.itlihe.gulimall.product.entity.CategoryEntity;
import com.itlihe.gulimall.product.service.CategoryBrandRelationService;
import com.itlihe.gulimall.product.service.CategoryService;
import com.itlihe.gulimall.product.vo.Catalog2Vo;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    // 这里用了泛型 相当于掉用baseMapper就是调用categoryDao
    private CategoryDao categoryDao;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    private RedissonClient redisson;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id 获取 Category 全部数据
     *
     * @param catelogId
     * @return
     */
    @Override
    public CategoryEntity selectByIdName(Long catelogId) {

        CategoryEntity categoryEntity = categoryDao.selectByIdName(catelogId);

        return categoryEntity;

    }

    /**
     * 修改分类表的同时修改品牌分类表
     *
     * @param category
     */
    @Transactional
    @Override
    public void updateByDetail(CategoryEntity category) {

        // 修改分类表
        categoryDao.updateById(category);

        // 修改品牌分类关联表
        if (!StringUtils.isEmpty(category.getName())) {
            categoryBrandRelationService.updateByDetail(category.getCatId(), category.getName());
        }

        // TODO 更新其他关联

    }

    /**
     * 查询一级分类 一级分类的parent_id  = 0
     *@Cacheable 代表当前方法的返回需要保存到缓存，如果缓存中有方法不调用
     *  一般还需要给缓存数据的值设置key,指定缓存分区
     *  1. 默认行为：
     *      缓存中存在，方法不调用
     *      key默认自动生成 ，"menus1" 只是设置分区
     *      缓存的value值，默认使用jdk序列化机制，将二进制的书数据存入到redis中
     *      过期时间是 -1 永不过期
     *  2. 自定义：
     *       指定生成的key 属性key key = "'level1Catalog'"
     *       指定缓存存活的时间 配置文件中配置redis:
     *                          time-to-live: 10000
     *       将数据保存为json格式
     * @return
     */
    // 加上单引号的原因是保证不是动态取值
    @Cacheable(value = "menus1",key = "#root.methodName")
    @Override
    public List<CategoryEntity> menus() {

        List<CategoryEntity> categoryEntities = categoryDao.mens();

        return categoryEntities;
    }

    /**
     * 将数据加入到缓存中
     * 1.缓存穿透
     * 当大量的请求，请求缓存中和数据库中都没有的数据时数据库会挂掉，解决方法给不存在的值设置为 null
     * 2.缓存击穿
     * 当缓存中一个热点值过期的时候，大量的访问直接访问数据库，会将数据库打挂掉，解决方法
     * 单体服务可以使用本地锁
     * 3.缓存雪崩
     * 当缓存中大量的值同时过期，大量打访问数据库，数据库还是会挂掉
     * 给数据设定随机时间过期
     *
     * @return
     */
    @Override
    public Map<String, List<Catalog2Vo>> getCatalogJson() {

        String catalogKey = "gulimall:product:CatalogJSON";
        /**
         * 缓存存入的数据需要是json类型的数据 ，取出的时候需要我们对应的数据。
         * 1.先判断 redis 中是否有数据，没有的话添加
         */
        String catalogJSON = (String) redisUtil.get("gulimall:product:CatalogJSON");

        // ValueOperations<String, String> ops = redisTemplate.opsForValue();
        // String catalogJSON = ops.get("CatalogJSON");

        // 判断redis中是否有数据
        if (StringUtils.isEmpty(catalogJSON)) {
            // 没有的话添加数据  当我们查询到数据之后直接将数据返回
            // 保证数据库查完之后，返回之前就将数据放入redis中，是一个原子操作  测试分布式锁
            Map<String, List<Catalog2Vo>> catalogJsonDB = getCatalogJsonDispersedLock();

            return catalogJsonDB;

        }
        // 取出的json数据转成我们需要的数据
        Map<String, List<Catalog2Vo>> map = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catalog2Vo>>>() {
        });

        return map;

    }


    /**
     * 使用redisson加上分布式锁
     * 问题； 如何保证缓存中的数据和数据库中的数据的一致性
     * @return
     */
    public Map<String, List<Catalog2Vo>> getCatalogJsonRedissonDispersedLock() {

        // 锁的命名规格；锁的粒度;保证缓存的是具体某个数据。
        RLock lock = redisson.getLock("proud-catalog-lock");

        // 加锁
        lock.lock();

        // 枷锁成功..执行业务保证原子性 这里需要try finally一下就算业务失败了,也要执行解锁
        Map<String, List<Catalog2Vo>> catalogJsonDB;
        try {
            catalogJsonDB = this.getDataFromDB();
        } finally {
            // 释放锁
            lock.unlock();
        }
        return catalogJsonDB;

    }


    /**
     * 原始加锁 分布式锁 最终形态：
     * 在获取锁 和释放锁的时候都要保证原子性
     *
     * @return
     */
    public Map<String, List<Catalog2Vo>> getCatalogJsonDispersedLock() {

        String dispersedLock = "gulimall:product:dispersedLock";
        // 1.占分布式锁
        String uuid = UUID.randomUUID().toString();
        // 2.设置过期时间必须保证和加锁是同步的，保证原子性
        boolean lock = redisTemplate.opsForValue().setIfAbsent(dispersedLock, uuid, 300, TimeUnit.SECONDS);
        // 判断是否枷锁成功
        if (lock) {
            log.info("加锁成功");
            // 枷锁成功..执行业务保证原子性 这里需要try finally一下就算业务失败了,也要执行解锁
            Map<String, List<Catalog2Vo>> catalogJsonDB;
            try {
                catalogJsonDB = getDataFromDB();
            } finally {
                //原子删锁 因此获取lock对比删除这几步必须是原子操作。官网对此也与解释，使用lua脚本解决这个问题：
                String script = "if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end";
                redisTemplate.execute(new DefaultRedisScript<Integer>(script, Integer.class)
                        , Arrays.asList(dispersedLock), uuid);
            }
            return catalogJsonDB;
        } else {
            // 加锁失败休眠100秒重试
            try {
                Thread.sleep(2000);//毫秒
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("分布式锁加锁失败重试中");
            return getCatalogJsonDispersedLock();

        }


    }

    /**
     * 本地锁
     * 获取三级分类的json数据 真正获取数据的方法 本地锁实现单个服务加锁的方式
     *
     * @return
     */
    public Map<String, List<Catalog2Vo>> getCatalogJsonDB() {

        // TODO 单体服务使用本地锁 这里会有个问题当我们第一个线程进来抢到锁，执行查询数据库的时候，如果在释放锁之后才将数据添加到缓存中会出现 (第一次放入会很慢)
        // TODO 可能会出现当第一个线程抢到锁查询之后，第二个线程直接在来抢锁这时候还没有将数据添加到缓存中，会出现查询两次数据库
        synchronized (this) {
            /**
             * 第一次得到锁之后应该将数据添加到redis中 之后抢到锁的请求要在判断一下redis中是否有数据
             */
            String catalogJSON = (String) redisUtil.get("gulimall:product:CatalogJSON");
            if (!StringUtils.isEmpty(catalogJSON)) {
                System.out.println("缓存不为null直接返回");
                Map<String, List<Catalog2Vo>> map = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catalog2Vo>>>() {
                });
                return map;
            }
            System.out.println("缓存未命中查询数据库");

            // 需要优化一下，循环查库效率会非常慢，直接查询出全部数据 然后通过stream将 过滤出来父分类 id == 分类id的值来分别查询出响应的二三级分类
            List<CategoryEntity> categoryEntitielist = categoryDao.selectAll();

            // 查询出所有的一级分类(优化查询不在调用数据库查询)
            // List<CategoryEntity> mens = categoryDao.mens();
            List<CategoryEntity> level1 = categoryEntitielist.stream().filter(categoryEntity -> {
                return categoryEntity.getParentCid() == 0L;
            }).collect(Collectors.toList());

            // 根据一级分类查询出全部的二级分类封装成我们需要的数据map类型的JSON数据
            Map<String, List<Catalog2Vo>> CatalogJson = level1.stream().collect(Collectors.toMap(k1 ->
                            k1.getCatId().toString()
                    , v1 -> {
                        // 根据一级分类的id 查询出对应的二级分类Id
                        // List<CategoryEntity> level2 = categoryDao.getlevel2(v1.getCatId());
                        List<CategoryEntity> level2 = categoryEntitielist.stream().filter(categoryEntity -> {
                            return categoryEntity.getParentCid().equals(v1.getCatId());
                        }).collect(Collectors.toList());
                        // catalogVo赋值
                        List<Catalog2Vo> catelog2Vos;
                        // 遍历所有的二级分类并赋值返回
                        catelog2Vos = level2.stream().map(v2 -> {
                            Catalog2Vo catalog2Vo = new Catalog2Vo(v1.getCatId().toString(), v2.getCatId().toString(), v2.getName(), null);
                            // 查询出所有的三级分类的集合
                            // List<CategoryEntity> level3s = categoryDao.getlevel3(v2.getCatId());
                            List<CategoryEntity> level3s = categoryEntitielist.stream().filter(categoryEntity -> {
                                return categoryEntity.getParentCid().equals(v2.getCatId());
                            }).collect(Collectors.toList());
                            List<Catalog2Vo.Catelog3Vo> Catelog3Vos = level3s.stream().map(level3 -> {
                                Catalog2Vo.Catelog3Vo Catelog3Vo = new Catalog2Vo.Catelog3Vo(v2.getCatId().toString(), level3.getCatId().toString(), level3.getName());
                                return Catelog3Vo;
                            }).collect(Collectors.toList());
                            //给Catelog2Vo中的三级分类集合赋值
                            catalog2Vo.setCatalog3List(Catelog3Vos);
                            // 返回遍历收集完成之后的 三级分类数据
                            return catalog2Vo;
                        }).collect(Collectors.toList());

                        // 返回收集完成的二级分类数据
                        return catelog2Vos;
                    }));
            // TODO 在查询出来数据之后直接将数据放入缓存，返回之前放入缓存
            String s = JSON.toJSONString(CatalogJson);
            // 保存到reids中 需要保存json数据
            //ops.set("CatalogJSON", s);
            // 设置过期时间是 一天
            redisUtil.set("gulimall:product:CatalogJSON", s, 1);
            // 返回封装好的json对象
            return CatalogJson;
        }


    }

    /**
     * 取数据库中提取数据
     *
     * @return
     */
    private Map<String, List<Catalog2Vo>> getDataFromDB() {

        String catalogJSON = (String) redisUtil.get("gulimall:product:CatalogJSON");
        if (!StringUtils.isEmpty(catalogJSON)) {
            System.out.println("缓存不为null直接返回");
            Map<String, List<Catalog2Vo>> map = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catalog2Vo>>>() {
            });
            return map;
        }
        System.out.println("缓存未命中查询数据库");

        // 需要优化一下，循环查库效率会非常慢，直接查询出全部数据 然后通过stream将 过滤出来父分类 id == 分类id的值来分别查询出响应的二三级分类
        List<CategoryEntity> categoryEntitielist = categoryDao.selectAll();

        // 查询出所有的一级分类(优化查询不在调用数据库查询)
        // List<CategoryEntity> mens = categoryDao.mens();
        List<CategoryEntity> level1 = categoryEntitielist.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid() == 0L;
        }).collect(Collectors.toList());

        // 根据一级分类查询出全部的二级分类封装成我们需要的数据map类型的JSON数据
        Map<String, List<Catalog2Vo>> CatalogJson = level1.stream().collect(Collectors.toMap(k1 ->
                        k1.getCatId().toString()
                , v1 -> {
                    // 根据一级分类的id 查询出对应的二级分类Id
                    // List<CategoryEntity> level2 = categoryDao.getlevel2(v1.getCatId());
                    List<CategoryEntity> level2 = categoryEntitielist.stream().filter(categoryEntity -> {
                        return categoryEntity.getParentCid().equals(v1.getCatId());
                    }).collect(Collectors.toList());
                    // catalogVo赋值
                    List<Catalog2Vo> catelog2Vos = null;
                    // 将二级分类的赋值给 catalogVo
                    if (level2 != null) {
                        // 遍历所有的二级分类并赋值返回
                        catelog2Vos = level2.stream().map(v2 -> {
                            Catalog2Vo catalog2Vo = new Catalog2Vo(v1.getCatId().toString(), v2.getCatId().toString(), v2.getName(), null);
                            // 查询出所有的三级分类的集合
                            // List<CategoryEntity> level3s = categoryDao.getlevel3(v2.getCatId());
                            List<CategoryEntity> level3s = categoryEntitielist.stream().filter(categoryEntity -> {
                                return categoryEntity.getParentCid().equals(v2.getCatId());
                            }).collect(Collectors.toList());
                            if (level3s != null) {
                                List<Catalog2Vo.Catelog3Vo> catelog3Vos = level3s.stream().map(level3 -> {
                                    Catalog2Vo.Catelog3Vo catelog3Vo = new Catalog2Vo.Catelog3Vo(v2.getCatId().toString(), level3.getCatId().toString(), level3.getName());
                                    return catelog3Vo;
                                }).collect(Collectors.toList());
                                //给Catelog2Vo中的三级分类集合赋值
                                catalog2Vo.setCatalog3List(catelog3Vos);
                            }
                            // 返回遍历收集完成之后的 三级分类数据
                            return catalog2Vo;
                        }).collect(Collectors.toList());
                    }
                    // 返回收集完成的二级分类数据
                    return catelog2Vos;
                }));
        // TODO 在查询出来数据之后直接将数据放入缓存，返回之前放入缓存
        String s = JSON.toJSONString(CatalogJson);
        // 保存到reids中 需要保存json数据
        //ops.set("CatalogJSON", s);
        // 设置过期时间是 一天
        redisUtil.set("gulimall:product:CatalogJSON", s, 1);
        // 返回封装好的json对象
        return CatalogJson;
    }

    /**
     * 获取三级id的完整路径
     * 先想清楚关系我们操作的表是属性分组表 其中 属性分组表中有关联者商品三级分类表中的catelogId 是三级分类的第三级、
     * 我们现在要回显表单数据显示，给前端回显 三级分类 的完整路径
     * 1. 先通过 属性分组表中的attrgroupId获取属性分组的全部信息，
     * 2.通过属性分组的实体类获取到 catelogId ，调用 商品三级分类的service层来获取对应的路径分类
     * 3.利用递归来获取对应的分类数据
     *
     * @param
     * @return
     */
    @Override
    public Long[] getByIdCateLogPath(Long catelogId) {

        // 创建一个集合用来接收 完整的路径
        List<Long> paths = new ArrayList<>();

        // 利用递归方法获取到分类id的父id和爷爷id
        paths = findParentPath(catelogId, paths);

        // 因为arraylist集合是按照顺序排列的所以要反转集合 利用 Collections 工具类
        Collections.reverse(paths);

        //由于上级方法需要返回的是一个Long[]数组,所以需要转化一下
        return paths.toArray(new Long[paths.size()]);
    }


    public List<Long> findParentPath(Long catelogId, List<Long> paths) {

        // 将每次查询到的 所属分类id 添加到集合中
        paths.add(catelogId);

        // 调用的是根据 catelogId 来获取全部的分组数据
        CategoryEntity category = this.getById(catelogId);

        //判断当前获取的对象中父类id是否等于0,如果不等于0,说明有父类id
        // ,重复调用当前递归方法查询出来并收集
        if (category.getParentCid() != 0) {
            // 进行递归查询
            findParentPath(category.getParentCid(), paths);
        }

        return paths;

    }

    /**
     * 查询出所有商品并且按照商品目录分成树状结构
     * 关键的两个点：
     * ①.商品的一级分类的父id是0
     * ②.商品的子分类的父id是父商品的分类id
     *
     * @return
     */
    @Override
    public List<CategoryEntity> listWithTree() {

        // 1.获取所有商品分类
        List<CategoryEntity> categoryList = baseMapper.selectList(null);

        // 2.查询出所有商品的一级目录
        List<CategoryEntity> menus1 = categoryList.stream().filter(categoryEntity -> {
            // 2.1过滤出一级目录
            return categoryEntity.getParentCid() == 0;
        }).map(menus -> {
            // 2.2将过滤出来的一级目录，通过一级目录获取它的子目录
            menus.setChildren(getChildren(menus, categoryList));
            return menus;
        }).sorted(
                // 2.3过滤出来之后将目录按照升序排列
                (menu1, menu2) -> menu1.getSort() - menu2.getSort()).collect(Collectors.toList());

        return menus1;
    }


    /**
     * 判断删除的菜单是否在别的地方被引用、
     * 删除的时候不能直接物理删除（直接将数据库中的数据删除）
     * 使用逻辑删除（数据库定义了该字段是否显示，1显示 0不显示）
     *
     * @param asList
     */
    @Override
    public void removeMenuById(List<Long> asList) {
        //TODO 1.检查当前菜单是否被其他地方引用，如果引用则不能直接删除

        baseMapper.deleteBatchIds(asList);

    }


    /**
     * 获取子目录
     * 方法：子商品的商品父类id等于当前商品的商品id
     *
     * @return
     */
    private List<CategoryEntity> getChildren(CategoryEntity currentCategory, List<CategoryEntity> all) {

        List<CategoryEntity> categoryChildren = all.stream().filter(Current -> {
            // 一级目录的catId等于商品的ParentCid说明该商品是他的子类 包装类使用equals比较
            return Current.getParentCid().equals(currentCategory.getCatId());
        }).map(categoryEntity ->
                // 递归查找二级目录的三级目录。
        {
            categoryEntity.setChildren(getChildren(categoryEntity, all));
            return categoryEntity;
        }).sorted(
                (menu1, menu2) -> (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort())
        ).collect(Collectors.toList());
        return categoryChildren;
    }


}