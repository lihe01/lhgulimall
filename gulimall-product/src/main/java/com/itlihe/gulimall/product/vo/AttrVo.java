package com.itlihe.gulimall.product.vo;

import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;

/**
 * @description: vo 是前端传过来数据我们将数据在这里封装，响应给前端
 * 视图对象，用于展示层，它的作用是把某个指定页面（或组件）的所有数据封装起来。
 * 可以和数据库一致也可以不一致 是用来封装请求和响应数据的
 * @author: LiHe
 * @date: 2022/11/25 23:21
 * @version: 1.0
 */
@Data
public class AttrVo {

    /**
     * 属性id
     */
    private Long attrId;
    /**
     * 属性名
     */
    private String attrName;
    /**
     * 是否需要检索[0-不需要，1-需要]
     */
    private Integer searchType;
    /**
     * 值类型[0-为单个值，1-可以选择多个值]
     */
    private Integer valueType;
    /**
     * 属性图标
     */
    private String icon;
    /**
     * 可选值列表[用逗号分隔]
     */
    private String valueSelect;
    /**
     * 属性类型[0-销售属性，1-基本属性，2-既是销售属性又是基本属性]
     */
    private Integer attrType;
    /**
     * 启用状态[0 - 禁用，1 - 启用]
     */
    private Long enable;
    /**
     * 所属分类
     */
    private Long catelogId;
    /**
     * 快速展示【是否展示在介绍上；0-否 1-是】，在sku中仍然可以调整
     */
    private Integer showDesc;

    /**
     * 属性分组id 在对应的属性数据库中没有但是关联了属性分组数据库，所以添加上方便后面的级联修改
     * 主要还是看接口文档中前端传过来的数据还包含一个 attrGroupId
     */
    private Long attrGroupId;
}
