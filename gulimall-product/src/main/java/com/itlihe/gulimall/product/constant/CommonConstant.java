package com.itlihe.gulimall.product.constant;

/**
 * 公共常量
 *
 * @description:
 * @author: LiHe
 * @date: 2022/11/24 20:00
 * @version: 1.0
 */
public class CommonConstant {

    public static final String PAGE = "page";
    public static final String ATTR_GROUP = "attrGroup";

}
