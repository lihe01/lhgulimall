package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {

    CategoryEntity selectByIdName(Long catelogId);

    List<CategoryEntity> mens();

    List<CategoryEntity> getlevel2(Long cataLogId);

    List<CategoryEntity> getlevel3(Long cataLogId);

    List<CategoryEntity> selectAll();
}
