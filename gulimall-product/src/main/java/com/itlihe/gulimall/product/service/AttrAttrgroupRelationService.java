package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.itlihe.gulimall.product.vo.AttrgroupRelationVo;

import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveaAttr(AttrAttrgroupRelationEntity attrAttrgroupRelationEntity);

    void saveAttrRelation(AttrgroupRelationVo[] vos);

}

