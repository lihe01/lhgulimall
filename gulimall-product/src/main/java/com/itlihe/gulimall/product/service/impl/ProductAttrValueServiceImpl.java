package com.itlihe.gulimall.product.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.product.dao.AttrDao;
import com.itlihe.gulimall.product.dao.ProductAttrValueDao;
import com.itlihe.gulimall.product.entity.AttrEntity;
import com.itlihe.gulimall.product.entity.ProductAttrValueEntity;
import com.itlihe.gulimall.product.service.ProductAttrValueService;
import com.itlihe.gulimall.product.vo.spusavevo.BaseAttrs;


@Service("productAttrValueService")
public class ProductAttrValueServiceImpl extends ServiceImpl<ProductAttrValueDao, ProductAttrValueEntity> implements ProductAttrValueService {

    @Autowired
    private ProductAttrValueDao productAttrValueDao;

    @Autowired
    private AttrDao attrDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProductAttrValueEntity> page = this.page(
                new Query<ProductAttrValueEntity>().getPage(params),
                new QueryWrapper<ProductAttrValueEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 将spu的基本属性添加到商品属性值表中
     * @param id
     * @param baseAttrs
     */
    @Override
    public void saveBaseAttrs(Long id, List<BaseAttrs> baseAttrs) {

        List<ProductAttrValueEntity> productAttrValueEntities = baseAttrs.stream().map((baseatte)->{

            // 还需要一个参数 attrName
            AttrEntity attrEntity = attrDao.selectById(baseatte.getAttrId());
            ProductAttrValueEntity productAttrValueEntity = new ProductAttrValueEntity();
            productAttrValueEntity.setSpuId(id);
            productAttrValueEntity.setAttrId(baseatte.getAttrId());
            productAttrValueEntity.setAttrValue(baseatte.getAttrValues());
            productAttrValueEntity.setAttrSort(baseatte.getShowDesc());
            productAttrValueEntity.setAttrName(attrEntity.getAttrName());

            return productAttrValueEntity;
        }).collect(Collectors.toList());

        productAttrValueDao.saveBaseAttrs(productAttrValueEntities);

    }

    /**
     * spu查询出所有规格参数
     * @param spuId
     * @return
     */
    @Override
    public List<ProductAttrValueEntity> selectBySpuId(Long spuId) {
        List<ProductAttrValueEntity> productAttrValueEntityList = productAttrValueDao.selectBySpuId(spuId);
        return productAttrValueEntityList;
    }

    /**
     * 批量更新规格数据
     * @param spuId
     * @param productAttrValueEntityList
     */
    @Override
    public void batchUpdateSpuAttr(Long spuId, List<ProductAttrValueEntity> productAttrValueEntityList) {

        //1.更具传递过来的spuId,先删除之前对应的所有属性
        productAttrValueDao.deleteBySpuId(spuId);

        //2.再进行保存新的规格数据
        List<ProductAttrValueEntity> productAttrValueEntities = productAttrValueEntityList.stream().map(item -> {
            item.setSpuId(spuId);
            return item;
        }).collect(Collectors.toList());
        /*保存*/
        productAttrValueDao.saveProductAttr(productAttrValueEntities);

    }

}