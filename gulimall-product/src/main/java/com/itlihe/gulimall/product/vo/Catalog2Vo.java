package com.itlihe.gulimall.product.vo;

import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: 二级和三级分类数据 将数据封装到vo中返回给
 * @author: LiHe
 * @date: 2022/12/7 8:38
 * @version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Catalog2Vo {

    private String catalog1Id; // 一级分类id
    private String id; //二级分类id
    private String name; // 二级分类名称
    private List<Catelog3Vo> catalog3List;


    // 创建一个静态内部类
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Catelog3Vo{

        private String catalog2Id; // 二级分类id
        private String id; // 三级分类id
        private String name;

    }







}
