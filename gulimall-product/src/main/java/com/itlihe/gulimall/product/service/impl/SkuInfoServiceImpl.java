package com.itlihe.gulimall.product.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.product.dao.SkuInfoDao;
import com.itlihe.gulimall.product.dao.SkuSaleAttrValueDao;
import com.itlihe.gulimall.product.dao.SpuInfoDescDao;
import com.itlihe.gulimall.product.entity.SkuImagesEntity;
import com.itlihe.gulimall.product.entity.SkuInfoEntity;
import com.itlihe.gulimall.product.entity.SpuInfoDescEntity;
import com.itlihe.gulimall.product.service.AttrGroupService;
import com.itlihe.gulimall.product.service.SkuImagesService;
import com.itlihe.gulimall.product.service.SkuInfoService;
import com.itlihe.gulimall.product.vo.skuitemvo.SkuItemSaleAttrVo;
import com.itlihe.gulimall.product.vo.skuitemvo.SkuItemVo;
import com.itlihe.gulimall.product.vo.skuitemvo.SpuItemAttrGroupVo;


@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Autowired
    private SkuInfoDao skuInfoDao;

    @Autowired
    private SpuInfoDescDao spuInfoDescDao;

    @Autowired
    private SkuImagesService skuImagesService;

    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private SkuSaleAttrValueDao skuSaleAttrValueDao;

    @Autowired
    private ThreadPoolExecutor MyExecutor;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                new QueryWrapper<SkuInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 将sku的基本信息保存到   skuinfo 中
     * @param skuInfoEntity
     */
    @Override
    public void saveInfo(SkuInfoEntity skuInfoEntity) {

    skuInfoDao.insert(skuInfoEntity);

    }

    /**
     * sku检索
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPageSkuSearching(Map<String, Object> params) {

        //key: '华为',//检索关键字
        // catelogId: 0,
        // brandId: 0,
        // min: 0,
        // max: 0
        String key = (String) params.get("key");
        String catalogId = (String) params.get("catalogId");
        String brandId = (String) params.get("brandId");
        String max = (String) params.get("max");
        String min = (String) params.get("min");

        // if (StringUtils.isNotEmpty(max)){
        //     // BigDecimal 精确地小数 用来解决   Double 出现
        //     BigDecimal bigDecimal = new BigDecimal(max);
        //     BigDecimal compare = new BigDecimal("0");
        //     if (bigDecimal.compareTo(compare) == 1){
        //
        //     }
        //
        // }

        List<SkuInfoEntity> skuInfoEntityList = skuInfoDao.queryPageByCondition(key,catalogId,brandId,max,min);

        IPage<SkuInfoEntity> page = this.page(new Query<SkuInfoEntity>().getPage(params));
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(skuInfoEntityList);
        return pageUtils;

    }

    /**
     * 根据spuId查询出sku的信息
     * @param spuId
     * @return
     */
    @Override
    public List<SkuInfoEntity> selectBySpuId(Long spuId) {

        List<SkuInfoEntity> skuInfoEntityList = skuInfoDao.selectBySpuId(spuId);

        return skuInfoEntityList;
    }

    /**
     * 根据skuId 查询出商品详细信息
     * 写vo的时候不要使用内部类，全部提取出来这样更方便，也方便之后写sql
     * @param skuId
     * @return
     */
    @Override
    public SkuItemVo queryItemInfo(Long skuId) throws ExecutionException, InterruptedException {

        SkuItemVo skuItemVo = new SkuItemVo();

        /**
         * 开启异步任务，需要线程池，将我们自己的线程池添加到异步任务的CompletableFuture
         */
        // 一. 开启一个异步应该下面的需要第一次异步进行之后，得到的返回值所有开启有返回值的异步
        CompletableFuture<SkuInfoEntity> skuInfo = CompletableFuture.supplyAsync(() -> {
            // 1.查询出skuInfo详情信息
            SkuInfoEntity skuInfoEntity = skuInfoDao.selectById(skuId);
            if (!ObjectUtils.isEmpty(skuInfoEntity)) {
                skuItemVo.setInfo(skuInfoEntity);
            }
            return skuInfoEntity;
        }, MyExecutor);

        // 下面的都需要等一执行完成之后获取到返回值才能运行
        CompletableFuture<Void> spuInfoDesc = skuInfo.thenAcceptAsync((skuInfoEntity) -> {
            // 2.查询出商品spuinfo先去信息

            SpuInfoDescEntity spuInfoDescEntity = spuInfoDescDao.selectById(skuInfoEntity.getSpuId());
            if (!ObjectUtils.isEmpty(spuInfoDescEntity)) {
                skuItemVo.setDeso(spuInfoDescEntity);
            }
        }, MyExecutor);

        CompletableFuture<Void> skuItemSaleAttr = skuInfo.thenAcceptAsync((skuInfoEntity) -> {
            // 4.查询出spu销售属性信息
            List<SkuItemSaleAttrVo> skuItemSaleAttrVos = skuSaleAttrValueDao.queryByspuId(skuInfoEntity.getSpuId());
            skuItemVo.setSaleAttr(skuItemSaleAttrVos);
        }, MyExecutor);


        CompletableFuture<Void> spuItemAttrGroup = skuInfo.thenAcceptAsync((skuInfoEntity) -> {
            // 5.查询出规格参数信息 关联表中left join查询
            Long catalogId = skuInfoEntity.getCatalogId();
            List<SpuItemAttrGroupVo> spuItemAttrGroupVos = attrGroupService.selectByspuId(skuInfoEntity.getSpuId(), catalogId);
            skuItemVo.setGroupAttrs(spuItemAttrGroupVos);
        }, MyExecutor);

        // 二 . 这个任务不需要获取返回值，所以在开一个异步
        CompletableFuture<Void> skuImages = CompletableFuture.runAsync(() -> {
            // 3.sku图片信息
            List<SkuImagesEntity> skuImagesEntities = skuImagesService.selectBySkuId(skuId);
            skuItemVo.setImagesEntities(skuImagesEntities);
        }, MyExecutor);


        // 等待全部运行完成
        CompletableFuture.allOf(skuInfo,spuInfoDesc,skuItemSaleAttr,spuItemAttrGroup,skuImages).get();

        return skuItemVo;
    }

    /**
     * 外连接查询出销售属性
     * @param spuId
     * @return
     */
    @Override
    public List<SkuItemSaleAttrVo> queryByspuId(Long spuId) {

        List<SkuItemSaleAttrVo> skuItemSaleAttrVoList = skuInfoDao.queryByspuId(spuId);


        return skuItemSaleAttrVoList;
    }

}