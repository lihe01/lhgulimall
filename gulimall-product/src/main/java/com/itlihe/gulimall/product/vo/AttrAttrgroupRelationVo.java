package com.itlihe.gulimall.product.vo;

import java.util.List;

import com.baomidou.mybatisplus.annotation.TableId;
import com.itlihe.gulimall.product.entity.AttrEntity;

import lombok.Data;

/**
 * @description:
 * @author: LiHe
 * @date: 2022/11/28 23:51
 * @version: 1.0
 */
@Data
public class AttrAttrgroupRelationVo {

    /**
     * 分组id
     */
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;

    /**
     * 属性实体类的集合
     */
    private List<AttrEntity> attrs;
}
