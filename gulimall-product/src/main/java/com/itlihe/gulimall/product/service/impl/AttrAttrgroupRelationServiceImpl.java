package com.itlihe.gulimall.product.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.itlihe.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.itlihe.gulimall.product.service.AttrAttrgroupRelationService;
import com.itlihe.gulimall.product.vo.AttrgroupRelationVo;


@Service("attrAttrgroupRelationService")
public class AttrAttrgroupRelationServiceImpl extends ServiceImpl<AttrAttrgroupRelationDao, AttrAttrgroupRelationEntity> implements AttrAttrgroupRelationService {

    @Autowired
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrAttrgroupRelationEntity> page = this.page(
                new Query<AttrAttrgroupRelationEntity>().getPage(params),
                new QueryWrapper<AttrAttrgroupRelationEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 级联保存
     *
     * @param attrAttrgroupRelationEntity
     */
    @Override
    public void saveaAttr(AttrAttrgroupRelationEntity attrAttrgroupRelationEntity) {

        attrAttrgroupRelationDao.saveaAttr(attrAttrgroupRelationEntity);

    }

    /**
     * 将属性分组和属性&属性分组表关联起来
     *
     * @param vos
     */
    @Override
    public void saveAttrRelation(AttrgroupRelationVo[] vos) {


        // 将传过来的vos 数据保存在 关联关系实体类中
        List<AttrAttrgroupRelationEntity> attrgroupRelationEntities = Arrays.asList(vos).stream().map((attrvo) -> {

            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(attrvo, relationEntity);
            return relationEntity;
        }).collect(Collectors.toList());

        // 使用foreach来保存list集合
        attrAttrgroupRelationDao.saveRelation(attrgroupRelationEntities);



    }

}