package com.itlihe.gulimall.product.service.impl;

import org.checkerframework.checker.optional.qual.Present;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.itlihe.gulimall.product.dao.AttrDao;
import com.itlihe.gulimall.product.dao.AttrGroupDao;
import com.itlihe.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.itlihe.gulimall.product.entity.AttrEntity;
import com.itlihe.gulimall.product.entity.AttrGroupEntity;
import com.itlihe.gulimall.product.service.AttrAttrgroupRelationService;
import com.itlihe.gulimall.product.service.AttrGroupService;
import com.itlihe.gulimall.product.vo.AttrAttrgroupRelationVo;
import com.itlihe.gulimall.product.vo.AttrgroupRelationVo;
import com.itlihe.gulimall.product.vo.skuitemvo.SpuItemAttrGroupVo;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {


    @Autowired
    private AttrGroupDao attrGroupDao;

    @Autowired
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Autowired
    private AttrDao attrDao;

    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * @param params
     * @param catelogId
     * @return
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params, long catelogId) {

        // 传入这个值是为了当成检索条件，比如我们想根据 商品的属性分组id检索或者根据关键词检索
        String key = (String) params.get("key");

        List<AttrGroupEntity> attrGroupList = attrGroupDao.selectByKeyAndCatelogId(key, catelogId);

        // 查询分页参数
        IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params));

        // 传入分页参数
        PageUtils pageUtils = new PageUtils(page);
        // 将查询出来的信息封装到分页工具类中
        pageUtils.setList(attrGroupList);

        return pageUtils;


    }

    /**
     * 删除属性&属性分类关联表中的数据
     *
     * @param relationVo
     */
    @Override
    public void deleteRelevancy(AttrgroupRelationVo[] relationVo) {


        // 因为前端传过来的是数组类型的数据， 先让数组转换成list 在将 relationVo 中的数据复制到 关联表的实体类中
        // 因为是数组所以使用stream 流的map遍历
        List<AttrAttrgroupRelationEntity> relationEntities = Arrays.asList(relationVo).stream().map((relationVoList) -> {

                    AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
                    BeanUtils.copyProperties(relationVoList, relationEntity);

                    return relationEntity;
                }
        ).collect(Collectors.toList());
        // 传入的是 关联表list集合
        attrAttrgroupRelationDao.deleteRelevancy(relationEntities);

    }

    /**
     * 根据分类id查询出属性分组信息加上属性信息
     *
     * @param catelogId
     * @return
     */
    @Override
    public List<AttrAttrgroupRelationVo> selectBYcatelogId(Long catelogId) {

        // 1.先查询出属性分组信息
        List<AttrGroupEntity> attrGroupEntityList = attrGroupDao.selectByCatelogIId(catelogId);
        // 2.在查询出对应的属性信息 需要响应的数据看出 响应的数据是 属性分组 attrGroupId 下的 属性 attrId 有关联的，不能使用分类id当做属性和属性分组的关联
        // List<AttrEntity> attrEntities = attrDao.selectByCatrlogId(catelogId);

        List<AttrAttrgroupRelationVo> relationVos = attrGroupEntityList.stream().map((attrGroupEntity) -> {

            // 2.获取当前分组关联的所有属性 先从关联表中获取属性id，在通过属性id 获取相应的属性信息（不能通过catelogId来获取相应的数据）
            List<AttrAttrgroupRelationEntity> relationEntities = attrAttrgroupRelationDao.selectByAttrgroupId(attrGroupEntity.getAttrGroupId());

            // 使用stream 或者 for循环来获取 AttrIds
            List<Long> attrIds = new ArrayList<>();
            for (AttrAttrgroupRelationEntity relationEntity : relationEntities) {
                if (!ObjectUtils.isEmpty(relationEntity) && relationEntity.getAttrId() != null) {
                    attrIds.add(relationEntity.getAttrId());
                }
            }
            // 根据属性 id 获取全部的属性信息
            List<AttrEntity> attrEntities = attrDao.selectByIds(attrIds);

            AttrAttrgroupRelationVo attrAttrgroupRelationVo = new AttrAttrgroupRelationVo();

            BeanUtils.copyProperties(attrGroupEntity, attrAttrgroupRelationVo);

            attrAttrgroupRelationVo.setAttrs(attrEntities);
            return attrAttrgroupRelationVo;
        }).collect(Collectors.toList());

        return relationVos;
    }

    /**
     * 连表查询出 groupName，
     * attrId
     * attrName
     * attrValue
     * @param spuId
     * @param catalogId
     * @return
     */
    @Override
    public List<SpuItemAttrGroupVo> selectByspuId(Long spuId, Long catalogId) {

        List<SpuItemAttrGroupVo> spuItemAttrGroupVos = attrDao.selectByspuId(spuId,catalogId);

        return spuItemAttrGroupVos;
    }


}