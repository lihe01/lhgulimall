package com.itlihe.gulimall.product.config;

import java.io.IOException;
import java.io.ObjectInputFilter;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description: redisson
 * @author: LiHe
 * @date: 2022/12/9 15:16
 * @version: 1.0
 */
@Configuration
public class MyRedisConfig {

    /**
     * 所有对redisson的使用都通过 redissonClient对象
     * @return
     * @throws IOException
     */
    @Bean(destroyMethod = "shutdown")
    public RedissonClient redisson(){

        // 1.创建配置位置
        Config config = new Config();
        //集群模式
        //config.useClusterServers().addNodeAddress("127.0.0.1:7004","127.0.0.1:7001");
        //单节点模式
        config.useSingleServer().setAddress("redis://192.168.253.100:6379");
        config.useSingleServer().setPassword("669900");

        // 2.根据config创建出 redissonClient 示例
        RedissonClient redissonClient = Redisson.create(config);
        return redissonClient;

    }

}
