package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.ProductAttrValueEntity;
import com.itlihe.gulimall.product.vo.spusavevo.BaseAttrs;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBaseAttrs(Long id, List<BaseAttrs> baseAttrs);

    List<ProductAttrValueEntity> selectBySpuId(Long spuId);

    void batchUpdateSpuAttr(Long spuId, List<ProductAttrValueEntity> productAttrValueEntityList);
}

