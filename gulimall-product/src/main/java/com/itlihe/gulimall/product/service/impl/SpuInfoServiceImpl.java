package com.itlihe.gulimall.product.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.myenum.ProductStatusEnum;
import com.itlihe.common.to.SkuHastStockVo;
import com.itlihe.common.to.SkuReductionTo;
import com.itlihe.common.to.SpuBoundTo;
import com.itlihe.common.to.es.SkuEsModule;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.common.utils.R;
import com.itlihe.gulimall.couponClient.CouponFeignClient;
import com.itlihe.gulimall.product.dao.SpuInfoDao;
import com.itlihe.gulimall.product.dao.SpuInfoDescDao;
import com.itlihe.gulimall.product.entity.BrandEntity;
import com.itlihe.gulimall.product.entity.CategoryEntity;
import com.itlihe.gulimall.product.entity.ProductAttrValueEntity;
import com.itlihe.gulimall.product.entity.SkuImagesEntity;
import com.itlihe.gulimall.product.entity.SkuInfoEntity;
import com.itlihe.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.itlihe.gulimall.product.entity.SpuInfoDescEntity;
import com.itlihe.gulimall.product.entity.SpuInfoEntity;
import com.itlihe.gulimall.product.service.AttrService;
import com.itlihe.gulimall.product.service.BrandService;
import com.itlihe.gulimall.product.service.CategoryService;
import com.itlihe.gulimall.product.service.ProductAttrValueService;
import com.itlihe.gulimall.product.service.SkuImagesService;
import com.itlihe.gulimall.product.service.SkuInfoService;
import com.itlihe.gulimall.product.service.SkuSaleAttrValueService;
import com.itlihe.gulimall.product.service.SpuImagesService;
import com.itlihe.gulimall.product.service.SpuInfoService;
import com.itlihe.gulimall.product.vo.spusavevo.Attr;
import com.itlihe.gulimall.product.vo.spusavevo.BaseAttrs;
import com.itlihe.gulimall.product.vo.spusavevo.Bounds;
import com.itlihe.gulimall.product.vo.spusavevo.Images;
import com.itlihe.gulimall.product.vo.spusavevo.Skus;
import com.itlihe.gulimall.product.vo.spusavevo.SpuSaveVo;
import com.itlihe.gulimall.searchClient.SearchFeignClient;
import com.itlihe.gulimall.wareClient.WareFeignClient;


@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Autowired
    private SpuInfoDao spuInfoDao;

    @Autowired
    private SpuInfoDescDao spuInfoDescDao;

    @Autowired
    private BrandService brandService;

    @Autowired
    private SpuImagesService spuImagesService;

    @Autowired
    private ProductAttrValueService productAttrValueService;

    @Autowired
    private SkuInfoService skuInfoService;

    @Autowired
    private SkuImagesService skuImagesService;

    @Autowired
    private SkuSaleAttrValueService skuSaleAttrValueService;

    @Autowired
    private CouponFeignClient couponFeignService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    @Autowired
    private WareFeignClient wareFeignClient;

    @Autowired
    private SearchFeignClient searchFeignClient;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }


    /**
     * 保存spu基本信息
     *
     * @param spuInfoEntity
     */
    private void saveBaseSpuInfo(SpuInfoEntity spuInfoEntity) {

        this.baseMapper.insert(spuInfoEntity);
    }

    /**
     * 保存spu信息
     *
     * @param spuInfoVo
     */
    @Transactional
    @Override
    public void saveSpuInfo(SpuSaveVo spuInfoVo) {

        // 1.保存spu基本信息到 pms_spu_info
        SpuInfoEntity spuInfoEntity = new SpuInfoEntity();
        BeanUtils.copyProperties(spuInfoVo, spuInfoEntity);
        // 基本属性还有 创建时间和更新时间需要添加
        spuInfoEntity.setCreateTime(new Date());
        spuInfoEntity.setUpdateTime(new Date());
        // 经过这一步生成了自增主键  不自己写的原因是不能生成自增的主键下面的数据无法添加到数据库
        // this.saveBaseSpuInfo(spuInfoEntity);
        // TODO 这里为什么不能生成主键
        spuInfoDao.saveBase(spuInfoEntity);


        //2,保存spu的描述到 pms_spu_info_desc
        /**
         * (批量插入数据，先将插入的数据set进实体类中获得list<entity>实体类，在通过foreach动态SQL来实现批量插入)如果主键是自增的情况下可以
         * 主键不是自增采用下面这种方法
         * 要关联表保存的时候要注意关联表的主键是否是自增
         */
        List<String> decripts = spuInfoVo.getDecript();
        SpuInfoDescEntity spuInfoDescEntity = new SpuInfoDescEntity();
        spuInfoDescEntity.setDecript(String.join(",", decripts));
        // pms_spu_info_desc表中的spu_id是pms_spu_info 的主键所以将spu的描述添加到表 spu_info_desc 中的时候还必须将主键也添加进去
        // 并且这个spuID不是主键 需要在实体类中说明
        spuInfoDescEntity.setSpuId(spuInfoEntity.getId());
        spuInfoDescDao.saveDesc(spuInfoDescEntity);

        // 3.保存spu的图片到 pms_spu_images
        List<String> images = spuInfoVo.getImages();
        spuImagesService.saveImages(spuInfoEntity.getId(), images);

        // 4.保存spu的规格参数到 pms_product_attr_value 需要格外添加的俩个数据是 spuId AttrName
        List<BaseAttrs> baseAttrs = spuInfoVo.getBaseAttrs();
        // AttrName的获取
        productAttrValueService.saveBaseAttrs(spuInfoEntity.getId(), baseAttrs);

        // 5.保存sku的基本信息

        // 5.4 保存sku的积分信息 跨服务调用 gulimall_sms => sms_spu_bounds (跨模块fegin调用)
        // 快模块调用是在公共的模块创建需要给前端或者后端响应的数据封装类 叫做 To To中包含了俩个服务之间调用需要传递的数据
        Bounds bounds = spuInfoVo.getBounds();
        SpuBoundTo spuBoundTo = new SpuBoundTo();
        BeanUtils.copyProperties(bounds, spuBoundTo);
        spuBoundTo.setSpuId(spuInfoEntity.getId());
        // 相当于在 product 中定义了 coupon 的service来实现远程调用
        R r = couponFeignService.save(spuBoundTo);
        if (r.getCode() != 0) {
            log.error("远程调用服务保存spu积分失败");
        }

        // 5.1 保存sku的基本信息 到  pms_sku_info
        List<Skus> skus = spuInfoVo.getSkus();
        if (skus != null && skus.size() > 0) {
            // 保存基本属性的时候会生成 sku_id 主键后面的保存都会用到 所以不适用stream 流封装成list集合
            skus.forEach(sku -> {
                SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
                //只提供 了这些                       还需要
                // private String skuName;          spuId
                // private BigDecimal price;        skuDesc
                // private String skuTitle;         skuDefaultImg
                //private String skuSubtitle;       catalogId    brandId select_count
                BeanUtils.copyProperties(sku, skuInfoEntity);
                skuInfoEntity.setSpuId(spuInfoEntity.getId());
                skuInfoEntity.setCatalogId(spuInfoEntity.getCatalogId());
                skuInfoEntity.setBrandId(spuInfoEntity.getBrandId());
                skuInfoEntity.setSaleCount(0L);
                // 获取默认的 图片url地址 也只能传入 sku 传过来的参数
                for (Images image : sku.getImages()) {
                    // 等于 1 是默认图片 可以将url地址传入 sku中
                    if (image.getDefaultImg() == 1) {
                        skuInfoEntity.setSkuDefaultImg(image.getImgUrl());
                    }
                }
                skuInfoService.saveInfo(skuInfoEntity);

                //5.2) sku的图片信息: pms_sku_images
                List<SkuImagesEntity> skuimagesEntities = sku.getImages().stream().map(img -> {
                    SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
                    skuImagesEntity.setSkuId(skuInfoEntity.getSkuId());
                    skuImagesEntity.setImgUrl(img.getImgUrl());
                    skuImagesEntity.setDefaultImg(img.getDefaultImg());
                    return skuImagesEntity;
                }).filter(entity -> {
                    //返回true就是需要，false就是剔除  没有图片的路径无须保存
                    return !StringUtils.isEmpty(entity.getImgUrl());
                }).collect(Collectors.toList());
                skuImagesService.saveBatchSpuImages(skuimagesEntities);

                // 5.3  保存sku 的规格参数到 pms_sku_sale_attr_value
                // 需要多保存一个 skuId
                List<Attr> attrs = sku.getAttr();
                List<SkuSaleAttrValueEntity> valueEntities = attrs.stream().map((attr) -> {
                    SkuSaleAttrValueEntity skuSaleAttrValueEntity = new SkuSaleAttrValueEntity();
                    BeanUtils.copyProperties(attr, skuSaleAttrValueEntity);
                    skuSaleAttrValueEntity.setSkuId(skuInfoEntity.getSkuId());
                    return skuSaleAttrValueEntity;
                }).collect(Collectors.toList());

                skuSaleAttrValueService.saveValue(valueEntities);

                // 5.4 保存优惠和满减信息 gulimall_sms->sms_sku_ladder\sms_sku_full_reduction\sms_member_price
                SkuReductionTo skuReductionTo = new SkuReductionTo();
                BeanUtils.copyProperties(sku, skuReductionTo);
                skuReductionTo.setSkuId(spuInfoEntity.getId());
                if (skuReductionTo.getFullCount() > 0 || skuReductionTo.getFullPrice()
                        .compareTo(new BigDecimal("0")) == 1) {
                    R save = couponFeignService.saveSkuReduction(skuReductionTo);
                    if (save.getCode() != 0) {
                        log.error("远程调用服务保存spu积分失败");
                    }
                }

            });
        }


    }

    /**
     * spu的检索功能
     *
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPageSearching(Map<String, Object> params) {
        /**
         *    key: '华为',//检索关键字
         *    catelogId: 6,//三级分类id
         *    brandId: 1,//品牌id
         *    status: 0,//商品状态
         */
        String key = (String) params.get("key");
        // 数据库中 是cataloglog
        String catalogId = (String) params.get("catalogId");
        String brandId = (String) params.get("brandId");
        String status = (String) params.get("status");

        List<SpuInfoEntity> spuInfoEntities  = spuInfoDao.ComplexRetrieval(key, catalogId, brandId, status);
        IPage<SpuInfoEntity> page = this.page(new Query<SpuInfoEntity>().getPage(params));
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(spuInfoEntities);

        return pageUtils;
    }

    /**
     * 商品上架功能
     * 我们将商品信息保存之后，会提交给 gulimall-search 让es来保存
     * 所以我们将需要保存的信息，封装成实体类放在 common中
     * spu 是基本属性，相当于和sku 一对多的关系
     * @param spuId
     */
    @Override
    public void up(Long spuId) {

        // 1.查询出对应的 sku 信息
        List<SkuInfoEntity> skuInfoEntityList = skuInfoService.selectBySpuId(spuId);

        List<Long> skuIds = new ArrayList<>();
        for (SkuInfoEntity skuInfoEntity : skuInfoEntityList) {
            Long skuId = skuInfoEntity.getSkuId();
            skuIds.add(skuId);
        }

        // 所有sku的可以被用来检索的规格属性信息  被检索的规格属性是一样的所有不需要循环查询
        // 一.查询出spuId对应的所有属性信息，包括可以被检索的和不被检索的
        List<ProductAttrValueEntity> productAttrValueEntities = productAttrValueService.selectBySpuId(spuId);
        List<Long> attrIds = productAttrValueEntities.stream().map(productAttrValueEntity -> {
            return productAttrValueEntity.getAttrId();
        }).collect(Collectors.toList());
        // 根据attrIdS 查询出可以被检索的attrIDs
        List<Long> attrIdS = attrService.getBYAttrIds(attrIds);

        HashSet<Long> hashSet = new HashSet<>(attrIdS);

        // 过滤不可以检索的属性,在收集成
        List<SkuEsModule.Attrs> attrsList = productAttrValueEntities.stream().filter(item -> {
            // contains（）当前列表若包含某元素，返回结果为true, 若不包含该元素，返回结果为false。
            // 一般使用set.contains（） 因为查询效率比list高很多
            return hashSet.contains(item.getAttrId());
        }).map(item -> {
            // 创建出 SkuEsModule 中的方法Attrs  这里就相当于把 Attrs 这个类放在了 SkuEsModule 中，这样写可能太好理解
            SkuEsModule.Attrs attrs = new SkuEsModule.Attrs();
            BeanUtils.copyProperties(item, attrs);
            return attrs;
        }).collect(Collectors.toList());

        // TODO 远程调用库存信息看是否有库存
        Map<Long, Boolean> map = null;
        try {
            // 我们现在查询出来的结果放在了R map中 取出来很麻烦，可以使用泛型定义取出的结果就是我们想要的数据
            R<List<SkuHastStockVo>> skusHastStock = wareFeignClient.getSkusHastStock(skuIds);
            // 相当于直接获取了想要的数据，为了获取想要的库存信息需要将list转成map
            List<SkuHastStockVo> data = skusHastStock.getData();
            map = data.stream().collect(Collectors.toMap(SkuHastStockVo::getSkuId, SkuHastStockVo::getHasStock));
        } catch (Exception e) {
            log.error("库存服务出现异常",e);
        }

        // 2.将查询出来的sku信息封装到SkuEsModule
        Map<Long, Boolean> finalMap = map;
        List<SkuEsModule> skuEsModules = skuInfoEntityList.stream().map(skuInfoEntity -> {
            SkuEsModule skuEsModule = new SkuEsModule();
            BeanUtils.copyProperties(skuInfoEntity,skuEsModule);
            // 不一样的属性：skuPrice、skuImg、hasStock、hotScore、brandName、brandImg、catalogName、attrs
            skuEsModule.setSkuPrice(skuInfoEntity.getPrice());
            skuEsModule.setSkuImg(skuInfoEntity.getSkuDefaultImg());
            // TODO 远程调用库存信息添加 hasStock、hotScore 简单起见设置成热度设置成0
            if (finalMap == null){
                skuEsModule.setHasStock(true);
            }else {
                skuEsModule.setHasStock(finalMap.get(skuInfoEntity.getSkuId()));
            }
            skuEsModule.setHotScore(0L);

            // 根据brandId查询出品牌信息
            BrandEntity brandEntity = brandService.selectByIdName(skuInfoEntity.getBrandId());
            skuEsModule.setBrandName(brandEntity.getName());
            skuEsModule.setBrandImg(brandEntity.getLogo());

            // 根据catalogId查询出 分类名称
            CategoryEntity categoryEntity = categoryService.getById(skuInfoEntity.getCatalogId());
            skuEsModule.setCatalogName(categoryEntity.getName());

            // 封装可以被检索的属性信息
            skuEsModule.setAttrs(attrsList);


            return skuEsModule;
        }).collect(Collectors.toList());

        // 3.存入es中  TODO 存入es中
        R productUp = searchFeignClient.productUp(skuEsModules);
        if (productUp.getCode() == 0){
            // 远程调用成功 修改spu中商品的状态和上架时间
            spuInfoDao.updateSpuStatus(spuId, ProductStatusEnum.PRODUCT_UP.getCode());
        }else {
            // 接口调用失败怎么办，TODO 接口的幂等性，重试机制
            /**
             * 1.Feign的调用流程 Feign有自动提示机制
             * 1.发送请求执行
             */
        }


    }


}