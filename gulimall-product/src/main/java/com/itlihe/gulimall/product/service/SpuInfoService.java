package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.SpuInfoEntity;
import com.itlihe.gulimall.product.vo.spusavevo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuSaveVo spuInfo);


    PageUtils queryPageSearching(Map<String, Object> params);

    void up(Long spuId);
}

