package com.itlihe.gulimall.product.config;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description: 创建出线程池配置
 * @author: LiHe
 * @date: 2022/12/14 21:51
 * @version: 1.0
 */
@Configuration
public class MyThreadConfig {

    @Autowired
    private MyThreadPoolProperties poolProperties;

    /**
     * 定义线程池 我们应该动态的来设置线程池的核心线程数量，最大线程池和最大存活时间
     * @return
     */
    @Bean
    public ThreadPoolExecutor threadPoolExecutor(){

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(poolProperties.getCorePoolSize(),
                poolProperties.getMaximumPoolSize(),
                poolProperties.getKeepAliveTime(),
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(5),
                Executors.defaultThreadFactory()
        );

        return threadPoolExecutor;
    }

}
