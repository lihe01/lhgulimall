package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

/**
 * 属性分组
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Repository
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {

    /**
     * 零散的简单类型数据 需要使用 @param注解让mybatis区分
     */
    List<AttrGroupEntity> selectByKeyAndCatelogId(@Param("key") String key, @Param("catelogId") long catelogId);

    List<AttrGroupEntity> selectByCatelogIId(Long catelogId);
}
