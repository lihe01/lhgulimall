package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 属性&属性分组关联
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
public interface AttrAttrgroupRelationDao extends BaseMapper<AttrAttrgroupRelationEntity> {

    void saveaAttr(AttrAttrgroupRelationEntity attrAttrgroupRelationEntity);

    AttrAttrgroupRelationEntity selectByAttrId(Long attrId);

    Integer countByAttrId(Long attrId);

    List<AttrAttrgroupRelationEntity> selectByAttrgroupId(Long attrgroupId);

    void deleteRelevancy(@Param("relationEntities") List<AttrAttrgroupRelationEntity> relationEntities);

    List<AttrAttrgroupRelationEntity> selectByGroupIds(@Param("attrGroupIds") List<Long> attrGroupIds);

    void saveRelation(@Param("attrgroupRelationEntities") List<AttrAttrgroupRelationEntity> attrgroupRelationEntities);
}
