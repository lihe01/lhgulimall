package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * sku图片
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {

    void saveBatchSpuImages(@Param("skuImagesEntityList") List<SkuImagesEntity> skuImagesEntityList);

    List<SkuImagesEntity> selectBySkuId(Long skuId);
}
