package com.itlihe.gulimall.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.itlihe.common.group.UpdateGroup;
import com.itlihe.common.valid.AddGroup;
import com.itlihe.common.valid.ListValue;
import com.itlihe.common.valid.UpdateStatusGroup;
import com.itlihe.common.valid.Updategroup;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.URL;

import lombok.Data;

/**
 * 品牌
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 品牌id  groups 分组校验只有当修改的时候校验才生效
     */
    @NotNull(message = "修改必须指定id", groups = {Updategroup.class})
    @Null(message = "新增的时候必须为空", groups = AddGroup.class)
    @TableId
    private Long brandId;
    /**
     * 品牌名
     *
     * @NotNull：用在基本类型的包装类型上面的属性注解，不能为null，但可以为empty
     * @NotEmpty：用在集合类上面的属性的注解，不能为null，而且长度必须大于0
     * @NotBlank：用在String上面属性的注解，不能为null，而且调用trim()后，长度必须大于0
     */
    @NotBlank(message = "品牌名不能为空", groups = {AddGroup.class, Updategroup.class})
    private String name;
    /**
     * 品牌logo地址
     */
    @NotEmpty(groups = {AddGroup.class})
    @URL(message = "logo必须是一个合法的url地址", groups = {AddGroup.class, UpdateGroup.class})
    private String logo;
    /**
     * 介绍
     */
    private String descript;
    /**
     * 显示状态[0-不显示；1-显示]
     */
    @NotNull(groups = {UpdateStatusGroup.class})
    @ListValue(vals = {0, 1}, groups = {AddGroup.class, UpdateStatusGroup.class})
    private Integer showStatus;
    /**
     * 检索首字母
     */
    @NotEmpty(groups = {AddGroup.class})
    @Pattern(regexp = "^[a-zA-Z]$", message = "检索首字母必须是一个字母", groups = {AddGroup.class, UpdateGroup.class})
    private String firstLetter;
    /**
     * 排序
     */
    @NotNull(groups = {AddGroup.class})
    @Min(value = 0, message = "排序必须大于等于0", groups = {AddGroup.class, UpdateGroup.class})
    private Integer sort;

}
