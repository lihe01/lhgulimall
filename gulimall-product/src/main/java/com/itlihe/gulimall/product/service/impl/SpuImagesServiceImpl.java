package com.itlihe.gulimall.product.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.product.dao.SpuImagesDao;
import com.itlihe.gulimall.product.entity.SpuImagesEntity;
import com.itlihe.gulimall.product.service.SpuImagesService;


@Service("spuImagesService")
public class SpuImagesServiceImpl extends ServiceImpl<SpuImagesDao, SpuImagesEntity> implements SpuImagesService {

    @Autowired
    private SpuImagesDao spuImagesDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuImagesEntity> page = this.page(
                new Query<SpuImagesEntity>().getPage(params),
                new QueryWrapper<SpuImagesEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 传入 spuId 和 images 集合来添加
     * @param id
     * @param images
     */
    @Override
    public void saveImages(Long id, List<String> images) {

        // 传入的是images集合使用批量插入的 foreach 动态sql
        if (images !=null && images.size()>0){
            List<SpuImagesEntity> spuImagesEntities = images.stream().map((image)->{
                SpuImagesEntity spuImagesEntity = new SpuImagesEntity();
                spuImagesEntity.setSpuId(id);
                spuImagesEntity.setImgUrl(image);
                return spuImagesEntity;
            }).collect(Collectors.toList());

            spuImagesDao.saveImages(spuImagesEntities);
        }

    }

}