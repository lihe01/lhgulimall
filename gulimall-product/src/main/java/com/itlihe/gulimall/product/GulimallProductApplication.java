package com.itlihe.gulimall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * 思路
 * 一.整合mybatis-plus
 *  （1）导入依赖
 *  （2）配置
 * 1.配置数据源
 * （1）导入数据库的驱动
 * （2）在application.yml配置数据源相关信息
 * 2.配置mybatis-Plus
 * （1）使用@MapperScan告诉springboot,mapper文件的位置
 * （2）在application.yml中告诉Mybatis-plus，sql映射文件的位置
 * (3)在application.yml中调整实体类id为自增主键
 * <p>
 * 二.逻辑删除
 * 1）配置全局的逻辑删除规则（省略）
 * 2）在低版本中3.1之前还需要配置逻辑删除的组件Bean
 * 3)给Bean实体类上的字段加上逻辑删除注解@TableLogic
 * <p>
 * 三.对象存储
 * 1）.引入 spring-cloud-starter-alicloud-oss 依赖
 * 2）.配置  alicloud:
 * access-key: LTAI5tE8ZuaAWw6VU3fXGCPs
 * secret-key: Oec0NdX6SrbudT4wP1wXlKtLIZGKJq
 * oss:
 * endpoint: oss-cn-shanghai.aliyuncs.com
 * 3）.自动注入 OSSClient 使用对象存储
 * 四.整合redisson
 *    1.导入依赖
 *         <dependency>
 *             <groupId>org.redisson</groupId>
 *             <artifactId>redisson</artifactId>
 *             <version>3.12.0</version>
 *         </dependency>
 *          2.配置redisson
 *五.使用 springCache 实现 缓存
 *      1.导入依赖 spring-boot-starter-cache、 spring-boot-starter-data-redis
 *      2.写配置
 *          (1)、自动配置了哪些 CacheAuroConfiguration会导入 RedisCacheConfiguration;
 *              自动配好了缓存管理器RedisCacheManager
 *          (2)、配置使用redis作为缓存
 *          spring:
 *               cache:
 *                  type: redis
 *      3、测试使用缓存
 *      @Cacheable: Triggers cache population.: 触发将数据保存到缓存的操作
 *      @CacheEvict: Triggers cache eviction.:触发将数据从缓存删除的操作
 *      @CachePut: Updates the cache without interfering with the method execution.: 不影响方法执行更新缓存
 *      @Caching: Regroups multiple cache operations to be applied on a method.: 组合以上多个操作
 *      @CacheConfig: Shares some common cache-related settings at class-level,: 在类级别共享缓存的相同配置
 *          直接加上注解 @EnableCaching 开启注解
 */
@EnableCaching
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.itlihe.gulimall.couponClient",
        "com.itlihe.gulimall.wareClient",
        "com.itlihe.gulimall.searchClient"})
@EnableTransactionManagement
@MapperScan("com.itlihe.gulimall.product.dao")
@ComponentScan({"com.itlihe.common",
        "com.itlihe.gulimall"})
public class GulimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallProductApplication.class, args);
    }

}
