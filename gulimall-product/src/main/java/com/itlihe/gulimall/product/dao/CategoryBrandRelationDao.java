package com.itlihe.gulimall.product.dao;

import java.util.List;

import com.itlihe.gulimall.product.entity.CategoryBrandRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 品牌分类关联
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
@Mapper
public interface CategoryBrandRelationDao extends BaseMapper<CategoryBrandRelationEntity> {

    List<CategoryBrandRelationEntity> ralationList(Long brandId);

    void saveRelation(CategoryBrandRelationEntity categoryBrandRelation);

    void updateByBrandId(@Param("brandId") Long brandId,@Param("name") String name);

    void updateByDetail(@Param("cateId") Long cateId,@Param("name") String name);

    List<CategoryBrandRelationEntity> selectByCateId(Long cateId);
}
