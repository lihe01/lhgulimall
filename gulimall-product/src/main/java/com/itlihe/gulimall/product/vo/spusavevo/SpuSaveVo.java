/**
 * Copyright 2022 bejson.com
 */
package com.itlihe.gulimall.product.vo.spusavevo;

import java.math.BigDecimal;
import java.util.List;

import com.itlihe.gulimall.product.vo.spusavevo.BaseAttrs;
import com.itlihe.gulimall.product.vo.spusavevo.Bounds;
import com.itlihe.gulimall.product.vo.spusavevo.Skus;

import lombok.Data;

/**
 * Auto-generated: 2022-11-29 14:54:35
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class SpuSaveVo {

    // 1.需要保存到 pms_spu_info 表中的信息
    private String spuName;
    private String spuDescription;
    private Long catalogId;
    private Long brandId;
    private BigDecimal weight;
    private Integer publishStatus;

    // 2.需要保持到 pms_spu_info_desc 表中的信息
    private List<String> decript;
    // 3.需要保持到 pms_spu_images 表中的信息
    private List<String> images;
    // 需要保存到 gulimall_sms => sms_spu_bounds (跨模块fegin调用)
    private Bounds bounds;
    // 4.需要保持到 pms_product_attr_value 表中并且是基本属性  AttrType = 1
    private List<BaseAttrs> baseAttrs;
    // 5.需要保持到 pms_sku_info 表中的信息
    private List<Skus> skus;

}