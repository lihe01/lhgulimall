/**
  * Copyright 2022 bejson.com 
  */
package com.itlihe.gulimall.product.vo.spusavevo;

import java.math.BigDecimal;

import lombok.Data;

/**
 * Auto-generated: 2022-11-29 14:54:35
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Bounds {

    private BigDecimal buyBounds;
    private BigDecimal growBounds;


}