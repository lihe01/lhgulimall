package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.SkuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * sku图片
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface SkuImagesService extends IService<SkuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBatchSpuImages(List<SkuImagesEntity> skuImagesEntities);

    List<SkuImagesEntity> selectBySkuId(Long skuId);
}

