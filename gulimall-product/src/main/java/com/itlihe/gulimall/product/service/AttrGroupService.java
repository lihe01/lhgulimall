package com.itlihe.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.itlihe.gulimall.product.entity.AttrGroupEntity;
import com.itlihe.gulimall.product.vo.AttrAttrgroupRelationVo;
import com.itlihe.gulimall.product.vo.AttrgroupRelationVo;
import com.itlihe.gulimall.product.vo.skuitemvo.SpuItemAttrGroupVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 10:58:20
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, long catelogId);


    void deleteRelevancy(AttrgroupRelationVo[] relationVo);

    List<AttrAttrgroupRelationVo> selectBYcatelogId(Long catelogId);

    List<SpuItemAttrGroupVo> selectByspuId(Long spuId, Long catalogId);
}

