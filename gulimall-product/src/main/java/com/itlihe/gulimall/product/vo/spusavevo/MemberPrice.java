/**
  * Copyright 2022 bejson.com
  */
package com.itlihe.gulimall.product.vo.spusavevo;

import java.math.BigDecimal;

import lombok.Data;

/**
 * Auto-generated: 2022-10-27 17:23:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class MemberPrice {

    private Long id;
    private String name;
    private BigDecimal price;
}
