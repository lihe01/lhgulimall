package com.itlihe.gulimall.product;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

// import com.aliyun.oss.ClientException;
// import com.aliyun.oss.OSSClient;
// import com.aliyun.oss.OSSException;
// import com.aliyun.oss.model.PutObjectRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itlihe.gulimall.product.dao.SkuSaleAttrValueDao;
import com.itlihe.gulimall.product.entity.BrandEntity;
import com.itlihe.gulimall.product.service.BrandService;
import com.itlihe.gulimall.product.service.impl.AttrGroupServiceImpl;
import com.itlihe.gulimall.product.vo.skuitemvo.SkuItemSaleAttrVo;
import com.itlihe.gulimall.product.vo.skuitemvo.SpuItemAttrGroupVo;

@SpringBootTest
class GulimallProductApplicationTests {

    @Autowired
    private BrandService brandService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    RedissonClient redissonClient;

    @Autowired
    private AttrGroupServiceImpl attrGroupService;

    @Autowired
    SkuSaleAttrValueDao skuSaleAttrValueDao;


    @Test
    public void selectBySku(){
        List<SkuItemSaleAttrVo> skuItemSaleAttrVos = skuSaleAttrValueDao.queryByspuId(11L);
        System.out.println(skuItemSaleAttrVos);
    }

    /**
     * 测试查询是否成功
     */
    @Test
    public void selectBuspuI() {
        List<SpuItemAttrGroupVo> spuItemAttrGroupVos = attrGroupService.selectByspuId(13L, 225L);
        System.out.println(spuItemAttrGroupVos);

    }

    /**
     * 测试 redisson是否可用
     */
    @Test
    public void redisson() {
        System.out.println(redissonClient);

    }


    /**
     * 测试连接redis
     */
    @Test
    public void testRedis() {

        // 添加值到redis中
        redisTemplate.opsForValue().set("name", "小米");

        // 从redis中获取值
        String name = redisTemplate.opsForValue().get("name");

        System.out.println("保存的值是" + name);
    }

    // /**
    //  * 使用阿里的依赖
    //  */
    // @Autowired
    // private OSSClient ossClient;
    //
    // /**
    //  * 测试文件上传
    //  */
    // @Test
    // public void upload(){
    //     // // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
    //     // String endpoint = "oss-cn-shanghai.aliyuncs.com";
    //     // // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    //     // String accessKeyId = "LTAI5tE8ZuaAWw6VU3fXGCPs";
    //     // String accessKeySecret = "Oec0NdX6SrbudT4wP1wXlKtLIZGKJq";
    //     // 填写Bucket名称，例如examplebucket。
    //     String bucketName = "lhgulimall";
    //     // 填写Object完整路径，完整路径中不能包含Bucket名称，上传的文件名
    //     String objectName = "20221121164833.jpg";
    //     // 填写本地文件的完整路径，例如D:\\localpath\\examplefile.txt。
    //     // 如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件。
    //     String filePath= "C:\\Users\\lh01\\Desktop\\20221121164833.jpg";
    //
    //
    //     // 创建OSSClient实例。
    //     // OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
    //
    //     try {
    //         // 创建PutObjectRequest对象。
    //         PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, new File(filePath));
    //         // 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
    //         // ObjectMetadata metadata = new ObjectMetadata();
    //         // metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
    //         // metadata.setObjectAcl(CannedAccessControlList.Private);
    //         // putObjectRequest.setMetadata(metadata);
    //
    //         // 上传文件。
    //         ossClient.putObject(putObjectRequest);
    //     } catch (OSSException oe) {
    //         System.out.println("Caught an OSSException, which means your request made it to OSS, "
    //                 + "but was rejected with an error response for some reason.");
    //         System.out.println("Error Message:" + oe.getErrorMessage());
    //         System.out.println("Error Code:" + oe.getErrorCode());
    //         System.out.println("Request ID:" + oe.getRequestId());
    //         System.out.println("Host ID:" + oe.getHostId());
    //     } catch (ClientException ce) {
    //         System.out.println("Caught an ClientException, which means the client encountered "
    //                 + "a serious internal problem while trying to communicate with OSS, "
    //                 + "such as not being able to access the network.");
    //         System.out.println("Error Message:" + ce.getMessage());
    //     } finally {
    //         if (ossClient != null) {
    //             ossClient.shutdown();
    //         }
    //     }


    // }

    @Test
    void contextLoads() {

        // BrandEntity brandEntity = new BrandEntity();
        //
        // brandEntity.setName("小米");
        // brandService.save(brandEntity);
        // System.out.println("保存成功");

        // 查询
        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));

        for (BrandEntity brandEntity : list) {
            System.out.println(brandEntity);
        }


    }

}
