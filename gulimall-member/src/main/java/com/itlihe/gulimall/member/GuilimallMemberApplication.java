package com.itlihe.gulimall.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 1.要实现微服务的远程调用
 *  1）.导入feign的依赖
 *  2）创建远程调用接口，接口使用注解 @FeignClient(value = "要调用的微服务在注册中心注册的名称")
 *  3）在启动类上添加注解 @EnableFeignClients(basePackages = "远程调用接口的包名")
 *  4.在要调用远程服务的 Controller 层导入 远程调用接口，（相当于充当了远程调用的service）
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.itlihe.gulimall.member.feign")
@EnableTransactionManagement
@MapperScan("com.itlihe.gulimall.member.dao")
public class GuilimallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuilimallMemberApplication.class, args);
    }

}
