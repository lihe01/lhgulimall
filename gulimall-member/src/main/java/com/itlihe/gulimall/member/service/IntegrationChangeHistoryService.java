package com.itlihe.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:24:09
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

