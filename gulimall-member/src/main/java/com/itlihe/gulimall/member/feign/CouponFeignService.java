package com.itlihe.gulimall.member.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.itlihe.common.utils.R;

/**
 *  声明feign接口充当远程调用coupon的接口
 *  value 的值是服务注册到注册中的名称
 */
@FeignClient(value = "gulimall-coupon")
public interface CouponFeignService {

    /**
     * 和要访问的方法，访问路径和要访问的方法签名要保持一致
     * @return
     */
    @GetMapping("/coupon/coupon/query/coupon")
    public R memberCoupon();
}
