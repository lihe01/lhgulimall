package com.itlihe.gulimall.productClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.itlihe.common.utils.R;

/**
 * 调用商品feign
 */
@FeignClient("gulimall-product")
public interface ProductFeginClient {

    @RequestMapping("product/skuinfo/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId);

}
