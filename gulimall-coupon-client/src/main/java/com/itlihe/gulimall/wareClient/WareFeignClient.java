package com.itlihe.gulimall.wareClient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.itlihe.common.to.SkuHastStockVo;
import com.itlihe.common.utils.R;

/**
 * 远程调用库存的feign
 */
@FeignClient("gulimall-ware")
public interface WareFeignClient {

    /**
     * 查询sku是否有库存
     * 传过来的skuID是json类型的数据，使用requestBody来实现转换
     */
    @PostMapping("/ware/waresku/hasstock")
    public R<List<SkuHastStockVo>> getSkusHastStock(@RequestBody List<Long> skuId);

}
