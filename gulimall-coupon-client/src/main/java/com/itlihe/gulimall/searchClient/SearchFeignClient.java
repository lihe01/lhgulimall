package com.itlihe.gulimall.searchClient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.itlihe.common.to.es.SkuEsModule;
import com.itlihe.common.utils.R;

/**
 * @description: 检索服务远程调用
 * @author: LiHe
 * @date: 2022/12/6 11:17
 * @version: 1.0
 */
@FeignClient("gulimall-search")
public interface SearchFeignClient {

    @PostMapping("/search/save/product")
    public R productUp(@RequestBody List<SkuEsModule> skuEsModules);

}
