package com.itlihe.gulimallsearch;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class GulimallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @Test
    void contextLoads() {
    }

    @Test
    public void contextLoadx(){
        // 1.创建ES客户端对象

        client = new RestHighLevelClient(RestClient.builder(
                new HttpHost(
                        "192.168.253.100",
                        9200,
                        "http"
                )
        ));

        System.out.println(client);

    }

}
