package com.itlihe.gulimallsearch.vo;

import lombok.Data;

@Data
public class BrandVo {
    private Long brandId;

    private String name;
}
