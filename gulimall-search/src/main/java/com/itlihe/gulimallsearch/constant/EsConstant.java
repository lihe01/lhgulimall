package com.itlihe.gulimallsearch.constant;

//将ES索引名字作为常量抽取出来
public class EsConstant {
    /**
     * sku数据在es中的索引
     */
    public static final String PRODUCT_INDEX = "product";

    /**
     * 每页显示的数量
     */
    public static final Integer PRODUCT_PAGESIZE =16;

}
