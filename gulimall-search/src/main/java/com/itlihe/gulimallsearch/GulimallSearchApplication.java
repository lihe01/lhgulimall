package com.itlihe.gulimallsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * 创建项目之后：1.导入es依赖和公共依赖
 *  2.公共依赖中有nacos配置中心和注册中心需要在 application.yml和bootstrap.properties 中配置
 *  3.启动类上添加 @EnableDiscoveryClient 同时启动不需要数据库舍去数据源
 *  4. 将es加入的spring容器中
 *
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class GulimallSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallSearchApplication.class, args);
    }

}
