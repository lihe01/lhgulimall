package com.itlihe.gulimallsearch.controller;

import javax.servlet.http.HttpServletRequest;

import org.elasticsearch.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.itlihe.gulimallsearch.service.MySearchService;
import com.itlihe.gulimallsearch.vo.SearchParamVo;
import com.itlihe.gulimallsearch.vo.SearchResult;

/**
 * @description: 搜索controller层
 * @author: LiHe
 * @date: 2022/12/12 13:58
 * @version: 1.0
 */
@Controller
public class SearchController {

    @Autowired
    private MySearchService mySearchService;

    /**
     * 自动将页面提交过来的请求参数封装成 SearchParamVo 对象在响应给前端 SearchResult 对象
     * @return
     */
    @GetMapping("/list.html")
    public String lisgPage(SearchParamVo paramVo, Model model, HttpServletRequest request){
        //根据页面传递的数据查询参数，去es中检索商品
        String queryString = request.getQueryString();
        paramVo.set_queryString(queryString);
        SearchResult reslut = mySearchService.searchInfo(paramVo);
        model.addAttribute("result",reslut);


        return "list";
    }



}
