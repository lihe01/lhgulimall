package com.itlihe.gulimallsearch.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.common.exception.GulimallExceptionEnum;
import com.itlihe.common.to.es.SkuEsModule;
import com.itlihe.common.utils.R;
import com.itlihe.gulimallsearch.service.ProductUpService;

import lombok.extern.slf4j.Slf4j;

/**
 * @description: 商品上架信息保存到es中
 * @author: LiHe
 * @date: 2022/12/6 10:19
 * @version: 1.0
 */
@Slf4j
@RequestMapping("/search/save")
@RestController
public class ProductUpController {

    @Autowired
    private ProductUpService productUpService;

    @PostMapping("/product")
    public R productUp(@RequestBody List<SkuEsModule> skuEsModules){

        boolean status = false;

        try {
            // 添加到es
            status = productUpService.prouctUp(skuEsModules);
        } catch (IOException e) {
            log.error("ElasticSaveController商品上架错误 {}",e);
            // 返回错误信息
            return R.error(GulimallExceptionEnum.PRODUCT_UP_EXCEPTION.getCode(),GulimallExceptionEnum.PRODUCT_UP_EXCEPTION.getMsg());
        }
        // 判断是否添加成功
        if (!status){
            return R.ok();
        }else {
            return R.error(GulimallExceptionEnum.PRODUCT_UP_EXCEPTION.getCode(),GulimallExceptionEnum.PRODUCT_UP_EXCEPTION.getMsg());
        }
    }


}
