package com.itlihe.gulimallsearch.service;

import com.itlihe.gulimallsearch.vo.SearchParamVo;
import com.itlihe.gulimallsearch.vo.SearchResult;

public interface MySearchService {

    SearchResult searchInfo(SearchParamVo searchParamVo);

}
