package com.itlihe.gulimallsearch.service.imp;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.itlihe.common.to.es.SkuEsModule;
import com.itlihe.gulimallsearch.config.MallElasticSearchConfig;
import com.itlihe.gulimallsearch.constant.EsConstant;
import com.itlihe.gulimallsearch.service.ProductUpService;

import lombok.extern.slf4j.Slf4j;

/**
 * @description:
 * @author: LiHe
 * @date: 2022/12/6 10:24
 * @version: 1.0
 */
@Slf4j
@Service("ProductUpServiceImp")
public class ProductUpServiceImp implements ProductUpService {

    @Autowired
    private RestHighLevelClient esRestClient;

    /**
     * 将上架的商品保存到es中
     * @param skuEsModules
     */
    @Override
    public boolean prouctUp(List<SkuEsModule> skuEsModules) throws IOException {
        //1.在es中建立索引，建立号映射关系（doc/json/product-mapping.json）

        //2. 在ES中保存这些数据
        BulkRequest bulkRequest = new BulkRequest();

        for (SkuEsModule skuEsModel : skuEsModules) {
            // 构造保存请求  索引
            IndexRequest indexRequest = new IndexRequest(EsConstant.PRODUCT_INDEX);
            indexRequest.id(skuEsModel.getSkuId().toString());
            String jsonString = JSON.toJSONString(skuEsModel);
            indexRequest.source(jsonString, XContentType.JSON);
            bulkRequest.add(indexRequest);
        }

        BulkResponse bulk = esRestClient.bulk(bulkRequest, MallElasticSearchConfig.COMMON_OPTIONS);

        //TODO 如果批量错误
        boolean hasFailures = bulk.hasFailures();

        List<String> collect = Arrays.stream(bulk.getItems()).map(BulkItemResponse::getId).collect(Collectors.toList());

        log.info("商品上架完成：{}", collect);

        return hasFailures;


    }
}
