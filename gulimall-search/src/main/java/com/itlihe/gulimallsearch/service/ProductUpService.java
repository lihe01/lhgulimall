package com.itlihe.gulimallsearch.service;

import java.io.IOException;
import java.util.List;

import com.itlihe.common.to.es.SkuEsModule;

public interface ProductUpService {

    boolean prouctUp(List<SkuEsModule> skuEsModules) throws IOException;

}
