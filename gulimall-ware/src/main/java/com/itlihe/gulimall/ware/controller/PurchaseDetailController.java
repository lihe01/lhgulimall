package com.itlihe.gulimall.ware.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.gulimall.ware.entity.PurchaseDetailEntity;
import com.itlihe.gulimall.ware.service.PurchaseDetailService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.R;


/**
 * 
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
@RestController
@RequestMapping("ware/purchasedetail")
public class PurchaseDetailController {
    @Autowired
    private PurchaseDetailService purchaseDetailService;

    /**
     * 查询采购需求
     */
    @RequestMapping("/list")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("ware:purchasedetail:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = purchaseDetailService.queryPagePurchasedetail(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("ware:purchasedetail:info")
    public R info(@PathVariable("id") Long id) {
            PurchaseDetailEntity purchaseDetail = purchaseDetailService.getById(id);

        return R.ok().put("purchaseDetail", purchaseDetail);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("ware:purchasedetail:save")
    public R save(@RequestBody PurchaseDetailEntity purchaseDetail) {
            purchaseDetailService.save(purchaseDetail);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("ware:purchasedetail:update")
    public R update(@RequestBody PurchaseDetailEntity purchaseDetail) {
            purchaseDetailService.updateById(purchaseDetail);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("ware:purchasedetail:delete")
    public R delete(@RequestBody Long[] ids) {
            purchaseDetailService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
