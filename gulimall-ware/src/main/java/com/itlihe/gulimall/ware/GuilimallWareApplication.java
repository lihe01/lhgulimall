package com.itlihe.gulimall.ware;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableDiscoveryClient
// 开启注解
@EnableTransactionManagement
// 扫描 mapper 的包注解
@MapperScan("com.itlihe.gulimall.ware.dao")
@EnableFeignClients(basePackages = {"com.itlihe.gulimall.productClient"})
public class GuilimallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuilimallWareApplication.class, args);
    }

}
