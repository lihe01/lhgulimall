package com.itlihe.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.ware.entity.WareSkuEntity;
import com.itlihe.gulimall.ware.vo.SkuHastStockVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageWareSku(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    List<SkuHastStockVo> getSkusHastStock(List<Long> skuId);
}

