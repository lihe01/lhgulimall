package com.itlihe.gulimall.ware.vo;

import lombok.Data;

/**
 * @description: 需要保存的值
 * @author: LiHe
 * @date: 2022/12/2 10:04
 * @version: 1.0
 */
@Data
public class PurchaseItemsDoneVo {

    //{itemId:1,status:4,reason:""}
    private Long itemId;//采购项id
    private Integer status;
    private String reason;//原因
}
