package com.itlihe.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.ware.entity.PurchaseDetailEntity;

import java.util.Map;

/**
 * 
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPagePurchasedetail(Map<String, Object> params);
}

