package com.itlihe.gulimall.ware.vo;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @description: 完成采购的vo
 * @author: LiHe
 * @date: 2022/12/2 10:03
 * @version: 1.0
 */
@Data
public class DoneVo {

    @NotNull
    private Long id;
    private List<PurchaseItemsDoneVo> items;

}
