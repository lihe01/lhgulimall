package com.itlihe.gulimall.ware.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.ware.dao.WareInfoDao;
import com.itlihe.gulimall.ware.entity.WareInfoEntity;
import com.itlihe.gulimall.ware.service.WareInfoService;


@Service("wareInfoService")
public class WareInfoServiceImpl extends ServiceImpl<WareInfoDao, WareInfoEntity> implements WareInfoService {

    @Autowired
    private WareInfoDao wareInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<WareInfoEntity> page = this.page(
                new Query<WareInfoEntity>().getPage(params),
                new QueryWrapper<WareInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 查询仓库列表信息
     *
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPageWareInfo(Map<String, Object> params) {

        String key = (String) params.get("key");

        List<WareInfoEntity> wareInfoEntityList = wareInfoDao.queryPageWareInfo(key);

        IPage<WareInfoEntity> page = this.page(new Query<WareInfoEntity>().getPage(params));
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(wareInfoEntityList);

        return pageUtils;
    }

}