package com.itlihe.gulimall.ware.dao;

import com.itlihe.gulimall.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
