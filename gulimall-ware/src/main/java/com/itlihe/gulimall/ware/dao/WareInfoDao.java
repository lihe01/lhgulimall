package com.itlihe.gulimall.ware.dao;

import java.util.List;

import com.itlihe.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {

    List<WareInfoEntity> queryPageWareInfo(String key);
}
