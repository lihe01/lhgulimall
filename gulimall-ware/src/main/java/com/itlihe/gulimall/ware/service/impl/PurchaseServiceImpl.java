package com.itlihe.gulimall.ware.service.impl;

import org.checkerframework.checker.fenum.qual.AwtCursorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.myenum.PurchaseDetailStatusEnum;
import com.itlihe.common.myenum.PurchaseStatusEnum;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.ware.dao.PurchaseDao;
import com.itlihe.gulimall.ware.dao.PurchaseDetailDao;
import com.itlihe.gulimall.ware.entity.PurchaseDetailEntity;
import com.itlihe.gulimall.ware.entity.PurchaseEntity;
import com.itlihe.gulimall.ware.entity.WareSkuEntity;
import com.itlihe.gulimall.ware.service.PurchaseService;
import com.itlihe.gulimall.ware.service.WareSkuService;
import com.itlihe.gulimall.ware.vo.DoneVo;
import com.itlihe.gulimall.ware.vo.MergeVo;


@Service("purchaseService")
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {

    @Autowired
    private PurchaseDao purchaseDao;

    @Autowired
    private PurchaseDetailDao purchaseDetailDao;

    @Autowired
    private WareSkuService wareSkuService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 查询未领取的采购单
     *
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPageUnreceivePurchase(Map<String, Object> params) {
        List<PurchaseEntity> purchaseEntityList = purchaseDao.queryPageUnreceivePurchase();

        IPage<PurchaseEntity> page = this.page(new Query<PurchaseEntity>().getPage(params));
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(purchaseEntityList);
        return pageUtils;

    }

    /**
     * 合并采购单
     * private List<Long> PurchaseItemsDoneVo
     *
     * @param mergeVo
     */
    @Transactional
    @Override
    public void mergePurchase(MergeVo mergeVo) {

        //当我们选择某一个采购单时,则这些采购项就合并到当前选择的采购单,如果没有选择任何采购单,则会新建一个采购单来添加这些采购项
        //首先判断当前传递过来的采购单id是否null,为null则新建一个采购单
        Long purchaseId = mergeVo.getPurchaseId();
        if (mergeVo.getPurchaseId() == null) {
            // 新建一个采购单
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            // 将新建采购单的状态 赋值到实体类中
            purchaseEntity.setStatus(PurchaseStatusEnum.CREATED.getCode());
            purchaseEntity.setCreateTime(new Date());
            purchaseEntity.setUpdateTime(new Date());

            purchaseDao.save(purchaseEntity);
            // 保存之后产生一个新的采购单 id
            purchaseId = purchaseEntity.getId();
        }

        // TODO 这里还需要判断当不是新建的采购单的时候，判断状态在新建和已分配状态
        PurchaseEntity purchaseEntity = purchaseDao.selectById(purchaseId);
        if (purchaseEntity.getStatus() == PurchaseStatusEnum.CREATED.getCode() || purchaseEntity.getStatus() == PurchaseStatusEnum.ASSIGNED.getCode()) {

            // 合并采购单其实就是修改当前采购需求中采购项的ID 并且改掉采购需求的状态 已分配
            List<Long> itemList = mergeVo.getItems();
            Long finalPurchaseId = purchaseId;

            List<PurchaseDetailEntity> purchaseDetailEntityList = itemList.stream().map((item) -> {
                PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
                purchaseDetailEntity.setPurchaseId(finalPurchaseId);
                purchaseDetailEntity.setId(item);
                // 采购项状态只有是 已分配 才能合并
                purchaseDetailEntity.setStatus(PurchaseDetailStatusEnum.ASSIGNED.getCode());
                return purchaseDetailEntity;
            }).collect(Collectors.toList());
            // 通过采购项id批量修改
            purchaseDetailDao.updateBatchById(purchaseDetailEntityList);

            //  每合并一次采购项到采购单,都应该对采购单的修改时间进行一次修改跟进
            PurchaseEntity purchaseEntityTwo = new PurchaseEntity();
            purchaseEntityTwo.setId(purchaseId);
            purchaseEntityTwo.setUpdateTime(new Date());

            purchaseDao.updateByPurchase(purchaseEntityTwo);

        }
    }

    /**
     * 用户端领取采购单 之后会显示正在采购中
     *
     * @param ids
     */
    @Transactional
    @Override
    public void received(List<Long> ids) {

        // 1.确认当前采购单的状态是新建或者已分配状态 将需要修改的订单封装成list集合
        List<PurchaseEntity> PurchaseEntityList = ids.stream().map(id -> {
            // 根据id查询出对象并返回
            PurchaseEntity purchaseEntity = purchaseDao.queryById(id);
            // 也可以直接在这判断采购单状态

            return purchaseEntity;
        }).filter((purchaseEntity) -> {
            // 过滤当返回 true 的时候，才保留
            if (purchaseEntity.getStatus() == PurchaseStatusEnum.CREATED.getCode()
                    || purchaseEntity.getStatus() == PurchaseStatusEnum.ASSIGNED.getCode()) {
                return true;
            } else {
                return false;
            }
            // 过滤之后剩下的订单都是状态是新建或者已分配的状态
        }).map((purchaseEntity) -> {
            // 订单员这时候操作就是修改订单的状态改成 已领取 同时将修改时间更新
            purchaseEntity.setStatus(PurchaseStatusEnum.RECEIVE.getCode());
            purchaseEntity.setUpdateTime(new Date());
            return purchaseEntity;
            // 将要修改的订单收集成list集合
        }).collect(Collectors.toList());

        purchaseDao.updateByStatus(PurchaseEntityList);

        //改变采购项的状态
        PurchaseEntityList.forEach(item ->{
            /* 通过采购单id获取采购项集合 */
            List<PurchaseDetailEntity> purchaseDetailEntityList = purchaseDetailDao.queryByPurchaseId(item.getId());

            List<PurchaseDetailEntity> purchaseDetailEntities = purchaseDetailEntityList.stream().map(Detail -> {
                PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
                purchaseDetailEntity.setId(Detail.getId());
                purchaseDetailEntity.setStatus(PurchaseDetailStatusEnum.BUYING.getCode());
                return purchaseDetailEntity;
            }).collect(Collectors.toList());
            //根据采购项Id批量修改采购项
            purchaseDetailDao.updateBatchById(purchaseDetailEntities);
        });



    }

    /**
     * 完成采购功能
     *
     * @param doneVo
     */
    @Transactional
    @Override
    public void done(DoneVo doneVo) {

        // 2.修改采购项状态为已完成
        // 创建标记位记录订单项是成功还是失败
        final Boolean[] Finalflag = {true};
        List<PurchaseDetailEntity> purchaseDetailEntityList = doneVo.getItems().stream().map((item) -> {
            // 判断当前订单是否采购失败 这里不用根据采购项ID查询出来的结果来修改的原因是，会在sql中当不需要修改的字段会为null 这时候不拼接所以不用修改
            // 如果这样写了就不需要在 setId.(item.getId)
            PurchaseDetailEntity purchaseDetailEntity = purchaseDetailDao.selectById(item.getItemId());
            if (item.getStatus() == PurchaseDetailStatusEnum.HASERROR.getCode()) {
                Finalflag[0] = false;
                purchaseDetailEntity.setStatus(PurchaseDetailStatusEnum.HASERROR.getCode());
            } else {
                // 采购成功需要入库
                purchaseDetailEntity.setStatus(PurchaseDetailStatusEnum.FINISH.getCode());
                wareSkuService.addStock(purchaseDetailEntity.getSkuId(),purchaseDetailEntity.getWareId(),purchaseDetailEntity.getSkuNum());
            }

            return purchaseDetailEntity;
        }).collect(Collectors.toList());
        purchaseDetailDao.updateBatchById(purchaseDetailEntityList);

        // 1.采购单状态是跟着采购项来确定的，只有当采购项全部的状态否是已完成，采购单才能是已完成，
        PurchaseEntity purchaseEntity = purchaseDao.selectById(doneVo.getId());
        purchaseEntity.setUpdateTime(new Date());
        purchaseEntity.setUpdateTime(new Date());
        purchaseEntity.setStatus(Finalflag[0] ? PurchaseStatusEnum.FINISH.getCode() :PurchaseStatusEnum.HASERROR.getCode());
        purchaseDao.updateByPurchase(purchaseEntity);
    }
}