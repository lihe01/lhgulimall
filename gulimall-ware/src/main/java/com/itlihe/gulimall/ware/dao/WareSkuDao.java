package com.itlihe.gulimall.ware.dao;

import java.util.List;

import com.itlihe.gulimall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品库存
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {

    List<WareSkuEntity> queryPageWareSku(@Param("wareId") String wareId,@Param("skuId") String skuId);

    List<WareSkuEntity> querySkuIdAndWareId(@Param("skuId") Long skuId, @Param("wareId") Long wareId);

    void sava(WareSkuEntity wareSkuEntity);

    void updateSkuNum(@Param("skuId") Long skuId, @Param("wareId") Long wareId, @Param("skuNum") Integer skuNum);

    List<WareSkuEntity> getSkusHastStock(@Param("skuIds") List<Long> skuIds);
}
