package com.itlihe.gulimall.ware.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.gulimall.ware.entity.WareSkuEntity;
import com.itlihe.gulimall.ware.service.WareSkuService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.R;
import com.itlihe.gulimall.ware.vo.SkuHastStockVo;


/**
 * 商品库存
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
@RestController
@RequestMapping("ware/waresku")
public class WareSkuController {
    @Autowired
    private WareSkuService wareSkuService;

    /**
     * 查询sku是否有库存
     * 传过来的skuID是json类型的数据，使用requestBody来实现转换
     */
    @PostMapping("/hasstock")
    public R<List<SkuHastStockVo>> getSkusHastStock(@RequestBody List<Long> skuId){
        // 需要返回的数据就是 sku_id 和 stock
        List<SkuHastStockVo> vos = wareSkuService.getSkusHastStock(skuId);

        R ok = R.ok();
        ok.setData(vos);

        return ok;
    }

    /**
     * 查询商品库存
     */
    @RequestMapping("/list")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("ware:waresku:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = wareSkuService.queryPageWareSku(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("ware:waresku:info")
    public R info(@PathVariable("id") Long id) {
            WareSkuEntity wareSku = wareSkuService.getById(id);

        return R.ok().put("wareSku", wareSku);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("ware:waresku:save")
    public R save(@RequestBody WareSkuEntity wareSku) {
            wareSkuService.save(wareSku);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("ware:waresku:update")
    public R update(@RequestBody WareSkuEntity wareSku) {
            wareSkuService.updateById(wareSku);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("ware:waresku:delete")
    public R delete(@RequestBody Long[] ids) {
            wareSkuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
