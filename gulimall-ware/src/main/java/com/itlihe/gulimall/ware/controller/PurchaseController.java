package com.itlihe.gulimall.ware.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itlihe.gulimall.ware.entity.PurchaseEntity;
import com.itlihe.gulimall.ware.service.PurchaseService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.R;
import com.itlihe.gulimall.ware.vo.DoneVo;
import com.itlihe.gulimall.ware.vo.MergeVo;


/**
 * 采购信息
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
@RestController
@RequestMapping("ware/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;

    /**
     * 完成采购功能
     * @param doneVo
     * @return
     */
    @PostMapping("/done")
    public R purchaseOk(@RequestBody DoneVo doneVo){

        purchaseService.done(doneVo);

        return R.ok();
    }

    /**
     * 领取采购单功能(用户端使用) 当我们将采购单合并之后，用户会通过 app 来领取采购单，这时候采购单状态改成 已领取
     */
    @PostMapping("/received")
    public R received(@RequestBody List<Long> ids){
        purchaseService.received(ids);
        return R.ok();
    }

    /**
     * /ware/purchase/merge
     * 、合并采购需求
     * 创建一个vo对象来接受{purchaseId: 8, PurchaseItemsDoneVo: [12, 11]}
     */
    @PostMapping("/merge")
    public R purchase(@RequestBody MergeVo mergeVo){

        purchaseService.mergePurchase(mergeVo);

        return R.ok();
    }


    /**
     * 查询未领取的采购单  查询出新建和已分配状态的列表
     * @return
     */
    @GetMapping("/unreceive/list")
    public R listUnreceive(@RequestParam Map<String, Object> params){

        PageUtils page = purchaseService.queryPageUnreceivePurchase(params);

        return R.ok().put("page", page);

    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    // 使用的是spring-c的权限控制
    // @RequiresPermissions("ware:purchase:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = purchaseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("ware:purchase:info")
    public R info(@PathVariable("id") Long id) {
            PurchaseEntity purchase = purchaseService.getById(id);

        return R.ok().put("purchase", purchase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("ware:purchase:save")
    public R save(@RequestBody PurchaseEntity purchase) {
            purchaseService.save(purchase);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("ware:purchase:update")
    public R update(@RequestBody PurchaseEntity purchase) {
            purchaseService.updateById(purchase);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("ware:purchase:delete")
    public R delete(@RequestBody Long[] ids) {
            purchaseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
