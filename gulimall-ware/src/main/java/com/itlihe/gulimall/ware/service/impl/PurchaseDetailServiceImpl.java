package com.itlihe.gulimall.ware.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.gulimall.ware.dao.PurchaseDetailDao;
import com.itlihe.gulimall.ware.entity.PurchaseDetailEntity;
import com.itlihe.gulimall.ware.service.PurchaseDetailService;


@Service("purchaseDetailService")
public class PurchaseDetailServiceImpl extends ServiceImpl<PurchaseDetailDao, PurchaseDetailEntity> implements PurchaseDetailService {


    @Autowired
    private PurchaseDetailDao purchaseDetailDao;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseDetailEntity> page = this.page(
                new Query<PurchaseDetailEntity>().getPage(params),
                new QueryWrapper<PurchaseDetailEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 查询采购需求
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPagePurchasedetail(Map<String, Object> params) {

        //          key: '华为',//检索关键字
        //          *status: 0,//状态
        //          *wareId: 1,//仓库id

        String key = (String) params.get("key");
        String wareId = (String) params.get("wareId");
        String status = (String) params.get("status");

        List<PurchaseDetailEntity> purchaseDetailEntityList = purchaseDetailDao.query(key,wareId,status);

        IPage<PurchaseDetailEntity> page = this.page(new Query<PurchaseDetailEntity>().getPage(params));
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(purchaseDetailEntityList);
        return pageUtils;

    }

}