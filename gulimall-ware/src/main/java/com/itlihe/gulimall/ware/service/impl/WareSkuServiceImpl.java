package com.itlihe.gulimall.ware.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.common.utils.Query;

import com.itlihe.common.utils.R;
import com.itlihe.gulimall.productClient.ProductFeginClient;
import com.itlihe.gulimall.ware.dao.WareSkuDao;
import com.itlihe.gulimall.ware.entity.WareSkuEntity;
import com.itlihe.gulimall.ware.service.WareSkuService;
import com.itlihe.gulimall.ware.vo.SkuHastStockVo;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Autowired
    private WareSkuDao wareSkuDao;

    @Autowired
    private ProductFeginClient productFeginClient;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                new QueryWrapper<WareSkuEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 查询商品库存
     *
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPageWareSku(Map<String, Object> params) {

        // wareId: 123  仓库id
        // skuId: 123   商品id
        String wareId = (String) params.get("wareId");
        String skuId = (String) params.get("skuId");

        List<WareSkuEntity> wareSkuEntityList = wareSkuDao.queryPageWareSku(wareId, skuId);
        IPage<WareSkuEntity> page = this.page(new Query<WareSkuEntity>().getPage(params));
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(wareSkuEntityList);

        return pageUtils;
    }

    /**
     * 将采购的信息入库
     *
     * @param skuId
     * @param wareId
     * @param skuNum
     */
    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNum) {

        // 判断是否有这个库 有的话加上库存 没有的话新增库
        List<WareSkuEntity> wareSkuEntityList = wareSkuDao.querySkuIdAndWareId(skuId, wareId);
        if (wareSkuEntityList == null || wareSkuEntityList.size() == 0) {
            WareSkuEntity wareSkuEntity = new WareSkuEntity();
            wareSkuEntity.setWareId(wareId);
            wareSkuEntity.setSkuId(skuId);
            wareSkuEntity.setStock(skuNum);
            wareSkuEntity.setStockLocked(0);
            //TODO 还可以用什么办法让异常出现以后不回滚？高级
            // 远程查询sku的名字,因为名字不是很重要所以希望调用失败的情况下不要回滚,所以使用try-catch来进行异常的捕获
            try {
                R skuInfo = productFeginClient.info(skuId);
                // 因为返回值是 R 所以是map 查询到的 skuInfo信息存入了Map中是key是skuInfo
                Map<String, Object> skuInfoPojo = (Map<String, Object>) skuInfo.get("skuInfo");
                // 查询成功 skuInfo.getCode() == 0
                if (skuInfo.getCode() == 0) {
                    wareSkuEntity.setSkuName((String) skuInfoPojo.get("skuName"));
                }
            } catch (Exception e) {
            }
            //保存
            wareSkuDao.sava(wareSkuEntity);
        } else {
            //2.有这条记录,则直接进行库存数量的修改即可
            wareSkuDao.updateSkuNum(skuId, wareId, skuNum);
        }
    }

    /**
     * 查询当前 skuId 对应的商品是否还有库存
     *
     * @param skuId
     * @return
     */
    @Override
    public List<SkuHastStockVo> getSkusHastStock(List<Long> skuId) {

        List<WareSkuEntity> wareSkuEntityList = wareSkuDao.getSkusHastStock(skuId);

        List<SkuHastStockVo> collect = wareSkuEntityList.stream().map(wareSkuEntity -> {
            SkuHastStockVo skuHastStockVo = new SkuHastStockVo();
            // 判断是否有库存 库存数等于当前库存减去锁定库存
            Integer quantity = wareSkuEntity.getStock() - wareSkuEntity.getStockLocked();
            skuHastStockVo.setSkuId(wareSkuEntity.getSkuId());
            skuHastStockVo.setHasStock(quantity == null ? false : quantity > 0);
            return skuHastStockVo;
        }).collect(Collectors.toList());
        // TODO 也可以这样写 遍历skuId根据id 查询出 Stock - StockLocked > 0的sku库存
        // select sum(stock - stock_locked) from wms_ware_sku where sku_id = #{skuId} 将查询出来

        return collect;

    }
}