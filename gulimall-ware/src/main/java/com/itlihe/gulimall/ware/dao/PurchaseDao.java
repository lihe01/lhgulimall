package com.itlihe.gulimall.ware.dao;

import java.util.List;

import com.itlihe.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 采购信息
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {

    List<PurchaseEntity> queryPageUnreceivePurchase();

    void save(PurchaseEntity purchaseEntity);

    void updateByPurchase(PurchaseEntity purchaseEntityTwo);

    PurchaseEntity queryById(Long id);

    void updateByStatus(@Param("purchaseEntityList") List<PurchaseEntity> purchaseEntityList);
}
