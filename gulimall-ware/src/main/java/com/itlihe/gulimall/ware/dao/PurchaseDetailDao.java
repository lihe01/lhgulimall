package com.itlihe.gulimall.ware.dao;

import java.util.List;

import com.itlihe.gulimall.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:26:19
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {

    List<PurchaseDetailEntity> query(@Param("key") String key, @Param("wareId") String wareId, @Param("status") String status);

    void updateBatchById(@Param("purchaseDetailEntityList") List<PurchaseDetailEntity> purchaseDetailEntityList);

    List<PurchaseDetailEntity> queryByPurchaseId(Long id);
}
