package com.itlihe.gulimall.ware.vo;

import java.util.List;

import lombok.Data;

/**
 * @description:  合并 vo
 * @author: LiHe
 * @date: 2022/12/1 15:23
 * @version: 1.0
 */
@Data
public class MergeVo {

    private Long purchaseId; //整单id
    private List<Long> items;//[1,2,3,4] //合并项集合
}
