package com.itlihe.common.myenum;

/**
 * @description: 采购单枚举类
 * @author: LiHe
 * @date: 2022/12/1 15:49
 * @version: 1.0
 */

public enum PurchaseStatusEnum {

    CREATED(0,"新建"),ASSIGNED(1,"已分配"),
    RECEIVE(2,"已领取"),FINISH(3,"已完成"),
    HASERROR(4,"有异常");

    private int code;
    private String msg;

    PurchaseStatusEnum(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
