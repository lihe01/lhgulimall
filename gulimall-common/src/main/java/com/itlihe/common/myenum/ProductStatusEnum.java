package com.itlihe.common.myenum;

/**
 * @description: 商品上架枚举类
 * @author: LiHe
 * @date: 2022/12/6 11:50
 * @version: 1.0
 */

public enum  ProductStatusEnum {

    PRODUCT_UP(1,"商品上架"),PRODUCT_OUT(0,"商品下架");

    private Integer code;
    private String depict;

    ProductStatusEnum(Integer code,String depict){
        this.code = code;
        this.depict = depict;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDepict() {
        return depict;
    }

    public void setDepict(String depict) {
        this.depict = depict;
    }
}
