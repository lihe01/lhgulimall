package com.itlihe.common.myenum;

/**
 * 商品枚举类 用来标记 基本属性和销售属性
 */
public enum ProductAttrEnum {

    ATTR_TYPE_BASE(1,"基本属性"),ATTR_TYPR_SALE(0,"销售属性");

    private Integer code;
    private String account;

    ProductAttrEnum(Integer code, String account){

        this.code=code;
        this.account=account;

    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
