package com.itlihe.common.to;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @description:
 * @author: LiHe
 * @date: 2022/11/30 9:56
 * @version: 1.0
 */
@Data
public class SpuBoundTo {

    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}
