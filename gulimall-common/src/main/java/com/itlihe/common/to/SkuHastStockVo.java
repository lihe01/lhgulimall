package com.itlihe.common.to;

import lombok.Data;

/**
 * @description: 需要返回的库存数据
 * @author: LiHe
 * @date: 2022/12/5 17:00
 * @version: 1.0
 */
@Data
public class SkuHastStockVo {

    private Long skuId;
    private Boolean hasStock;
}
