package com.itlihe.common.to;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

/**
 * @description:
 * @author: LiHe
 * @date: 2022/11/30 11:35
 * @version: 1.0
 */
@Data
public class SkuReductionTo {

    // 1.sms_sku_full_reduction
    private Long skuId;
    private Integer fullCount;
    private BigDecimal discount;
    private Integer countStatus;

    // 2.sms_sku_full_reduction
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private Integer priceStatus;

    //3、sms_member_price
    private List<MemberPrice> memberPrice;
}
