package com.itlihe.common.to.es;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

/**
 * @description: 需要保存到es的sku信息
 * @author: LiHe
 * @date: 2022/12/5 14:39
 * @version: 1.0
 */
@Data
public class SkuEsModule {

    private Long skuId;

    private Long spuId;

    /**
     * 标题
     */
    private String skuTitle;

    /**
     * 价格
     */
    private BigDecimal skuPrice;

    /**
     * 图片
     */
    private String skuImg;

    /**
     * 销量
     */
    private Long saleCount;

    /**
     * 是否拥有库存
     */
    private Boolean hasStock;

    /**
     * 热度评分
     */
    private Long hotScore;

    /**
     * 品牌id
     */
    private Long brandId;

    /**
     * 分类ID
     */
    private Long catalogId;

    private String brandName;

    private String brandImg;

    private String catalogName;

    /**
     * 属性信息
     */
    private List<Attrs> attrs;

    /**
     *  检索属性
     *  为了保证其他第三方的工具可以对他序列化和反序列化需要实现接口 Serializable
     */
    @Data
    public static class Attrs implements Serializable {
        private Long attrId;

        private String attrName;

        private String attrValue;
    }

}
