package com.itlihe.common.to;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @description: 会员价格
 * @author: LiHe
 * @date: 2022/11/30 11:35
 * @version: 1.0
 */
@Data
public class MemberPrice {
    private Long id;
    private String name;
    private BigDecimal price;
}
