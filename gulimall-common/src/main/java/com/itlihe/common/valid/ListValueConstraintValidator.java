package com.itlihe.common.valid;


import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 自定义校验器
 */

public class ListValueConstraintValidator implements ConstraintValidator<com.itlihe.common.valid.ListValue,Integer> {//泛型指定要校验ListValue注解,要校验的类型是Integer

    //用于存放规定的校验信息值
    private Set<Integer> set = new HashSet<>();

    //初始化方法,会将指定的这个注解的详细信息给我们拿到(注解需要规定的校验的值)
    @Override
    public void initialize(com.itlihe.common.valid.ListValue constraintAnnotation) {

        //说明我们给定的值必须是指定的这些信息
        int[] vals = constraintAnnotation.vals();
        //遍历传进来的vals中的值,并且添加到set中,用于后期进行校验判断传进来的值是否包含在我们指定的这些值中
        if (vals != null){
            for (int val : vals){
                set.add(val);
            }
        }

    }

    /**
     *
     * @param value  传进来的需要校验的值
     * @param constraintValidatorContext
     * @return
     */
    //判断是否校验成功
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext constraintValidatorContext) {
        //判断传进来需要判断的值包不包含在set我们规定的值中,包含就返回true,否则返回falss
        return set.contains(value);
    }
}
