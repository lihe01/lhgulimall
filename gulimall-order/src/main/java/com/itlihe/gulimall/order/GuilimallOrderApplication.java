package com.itlihe.gulimall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class GuilimallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuilimallOrderApplication.class, args);
    }

}
