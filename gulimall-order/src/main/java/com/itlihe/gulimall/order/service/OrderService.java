package com.itlihe.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itlihe.common.utils.PageUtils;
import com.itlihe.gulimall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author lihe
 * @email lihe@gmail.com
 * @date 2022-11-16 13:20:56
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

